package it.unisalento.pps1920.carsharing.model;

public class FotoAuto {

    private int id;
    private Modello modello;
    private Marca marca;
    private byte[] foto;

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

}
