package it.unisalento.pps1920.carsharing.model;

public class Categoria {

    private int id;
    private String dimensioni;
    private Modello modello;

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getDimensioni() {
        return dimensioni;
    }

    public void setDimensioni(String nome) {
        this.dimensioni = nome;
    }

    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }
}
