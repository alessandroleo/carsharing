package it.unisalento.pps1920.carsharing.model;

public class Accessorio {

    private int id;
    private String nome;
    private int prezzo;
    private Mezzo mezzo;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPrezzo() {return prezzo; }

    public void setPrezzo(int prezzo) {this.prezzo= prezzo; }

    public Mezzo getMezzo() {
        return mezzo;
    }

    public void setMezzo(Mezzo mezzo) {
        this.mezzo = mezzo;
    }

}
