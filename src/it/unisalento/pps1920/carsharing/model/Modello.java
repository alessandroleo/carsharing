package it.unisalento.pps1920.carsharing.model;

public class Modello {

    private int id;
    private String nome;
    private int numero_posti;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNumero_posti() {
        return numero_posti;
    }

    public void setNumero_posti(int numero_posti) {
        this.numero_posti = numero_posti;
    }

    @Override
    public String toString() {
        return nome;
    }
}
