package it.unisalento.pps1920.carsharing.model;

import java.util.Date;

public class RichiestaCondivisione {

    private int id;
    private Date data;
    private String stato;
    private Utente cliente;
    private Prenotazione prenotazione;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) { this.data = data; }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public Utente getCliente() {
        return cliente;
    }

    public void setCliente(Utente cliente) { this.cliente = cliente; }

    public Prenotazione getPrenotazione() {
        return prenotazione;
    }

    public void setPrenotazione(Prenotazione prenotazione) {
        this.prenotazione = prenotazione;
    }


}
