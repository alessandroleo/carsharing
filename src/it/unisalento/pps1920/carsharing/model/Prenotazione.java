package it.unisalento.pps1920.carsharing.model;

import java.util.ArrayList;

public class Prenotazione {

    private int id;
    private String data_prenotazione;
    private String data_inizio;
    private String data_fine;
    private int num_posti_occupati;
    private Utente cliente;
    private Mezzo mezzo;
    private Localita localita;
    private Stazione stazione_partenza;
    private Stazione stazione_arrivo;
    private ArrayList<Accessorio> accessori;
    private int prezzoFinale;
    private int pagato;
    private int accessoriato;
    private int full;
    private int sharable;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData_prenotazione() {
        return data_prenotazione;
    }

    public void setData_prenotazione(String data_prenotazione) {
        this.data_prenotazione = data_prenotazione;
    }

    public String getData_inizio() {
        return data_inizio;
    }

    public void setData_inizio(String data_inizio) {
        this.data_inizio = data_inizio;
    }

    public String getData_fine() {
        return data_fine;
    }

    public void setData_fine(String data_fine) { this.data_fine = data_fine; }

    public int getNum_posti_occupati() {
        return num_posti_occupati;
    }

    public void setNum_posti_occupati(int num_posti_occupati) {
        this.num_posti_occupati = num_posti_occupati;
    }

    public Utente getCliente() {
        return cliente;
    }

    public void setCliente(Utente cliente) {
        this.cliente = cliente;
    }

    public Mezzo getMezzo() {
        return mezzo;
    }

    public void setMezzo(Mezzo mezzo) {
        this.mezzo = mezzo;
    }

    public Localita getLocalita() {
        return localita;
    }

    public void setLocalita(Localita localita) {
        this.localita = localita;
    }

    public Stazione getStazione_partenza() {
        return stazione_partenza;
    }

    public void setStazione_partenza(Stazione stazione_partenza) {
        this.stazione_partenza = stazione_partenza;
    }

    public Stazione getStazione_arrivo() {
        return stazione_arrivo;
    }

    public void setStazione_arrivo(Stazione stazione_arrivo) {
        this.stazione_arrivo = stazione_arrivo;
    }

    public int getPrezzoFinale() {
        return prezzoFinale;
    }

    public void setPrezzoFinale(int prezzoFinale) {
        this.prezzoFinale = prezzoFinale;
    }

    public ArrayList<Accessorio> getAccessori() {
        return accessori;
    }

    public void setAccessori(ArrayList<Accessorio> accessori) {
        this.accessori = accessori;
    }

    public int getPagato() {return pagato;}

    public void setPagato(int pagato) {this.pagato = pagato; }

    public int getAccessoriato() {return accessoriato;}

    public void setAccessoriato(int accessoriato) {this.accessoriato = accessoriato; }

    public int getFull() {return full;}

    public void setFull(int full) {this.full = full; }

    public int getSharable() {return sharable;}

    public void setSharable(int sharable) {this.sharable = sharable; }

}
