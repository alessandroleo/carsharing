package it.unisalento.pps1920.carsharing.model;


import java.io.File;

public class Utente {

    private int id;
    private String nome;
    private String cognome;
    private String password;
    private String email;
    private String telefono;
    private String residenza;
    private int eta;
    private byte[] foto;
    private File input;
    private String ruolo;
    private Stazione stazione;

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() { return telefono; }

    public void setTelefono(String telefono) { this.telefono = telefono; }

    public String getResidenza() { return residenza; }

    public void setResidenza(String residenza) { this.residenza = residenza; }

    public int getEta() { return eta; }

    public void setEta(int eta) { this.eta = eta; }

    public byte[] getFoto() { return foto; }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public File getInput() { return input; }

    public void setInput(File input) {
        this.input = input;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public Stazione getStazione()  { return stazione; }

    public void setStazione(Stazione stazione) { this.stazione = stazione; }
}


