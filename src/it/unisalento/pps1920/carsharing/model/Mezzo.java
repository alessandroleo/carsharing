package it.unisalento.pps1920.carsharing.model;

public class Mezzo {

    private int id;
    private String targa;
    private Modello modello;
    private Marca marca;
    private Motorizzazione motorizzazione;
    private int prezzo;
    private byte[] foto;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTarga() {
        return targa;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    public Modello getModello() {
        return modello;
    }

    public void setModello(Modello modello) {
        this.modello = modello;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Motorizzazione getMotorizzazione() {
        return motorizzazione;
    }

    public void setMotorizzazione(Motorizzazione motorizzazione) {
        this.motorizzazione = motorizzazione;
    }

    public int getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(int prezzo) {
        this.prezzo = prezzo;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }



}
