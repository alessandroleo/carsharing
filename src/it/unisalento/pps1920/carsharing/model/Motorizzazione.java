package it.unisalento.pps1920.carsharing.model;

public class Motorizzazione {

    private int id;
    private String tipo;

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
