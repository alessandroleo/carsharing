package it.unisalento.pps1920.carsharing.service;

import it.unisalento.pps1920.carsharing.dao.mysql.UtenteDAO;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Session;

public class UserService {

    private UtenteDAO utenteDAO = new UtenteDAO();

    public Utente login(String email, String password) {
        Utente u = utenteDAO.findByEmailAndPassword(email, password);

        if (u != null) {
            Session.getInstance().put(Session.LOGGED_USER, u);
        }

        return u;
    }

    public void logout() {
        Session.getInstance().remove(Session.LOGGED_USER);
    }
}
