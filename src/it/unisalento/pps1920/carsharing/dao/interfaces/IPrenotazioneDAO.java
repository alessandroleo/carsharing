package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.Prenotazione;
import it.unisalento.pps1920.carsharing.model.Utente;

import java.util.ArrayList;

public interface IPrenotazioneDAO extends IBaseDAO<Prenotazione> {

    public ArrayList<Prenotazione>  findAllByClienteId(int utenteId);

    public ArrayList<Prenotazione> sortByData();

    public ArrayList<Prenotazione> sortByStazione();

    public ArrayList<Prenotazione> sortByMezzo();

    public ArrayList<Prenotazione> findAllByStazioneId(int stazione_id);

    public int checkPrenotazione(Prenotazione prenotazione);

    public ArrayList<Utente> findAllClientiByPrenotazioneId(int id);

    public void addNewCliente (int clienteId, int prenotazioneId);

    public void deleteByClienteId (int clienteId, int prenotazioneId);
}
