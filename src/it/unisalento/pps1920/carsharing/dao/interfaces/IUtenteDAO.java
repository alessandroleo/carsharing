package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.Utente;

public interface IUtenteDAO extends IBaseDAO<Utente> {

    public Utente findByEmailAndPassword(String email, String password);

}
