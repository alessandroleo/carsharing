package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.Localita;

import java.util.ArrayList;

public interface IBaseDAO<T> {

    public T findById(int id);

    public ArrayList<T> findAll();

    public void save(T var);

    public void delete(T var);

}
