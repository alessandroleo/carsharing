package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.Marca;

public interface IMarcaDAO extends IBaseDAO<Marca> {
}
