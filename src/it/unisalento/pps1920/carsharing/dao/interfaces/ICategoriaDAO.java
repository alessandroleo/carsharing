package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.Categoria;

public interface ICategoriaDAO extends IBaseDAO<Categoria> {
}
