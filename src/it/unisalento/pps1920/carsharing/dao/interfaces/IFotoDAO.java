package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.FotoAuto;

public interface IFotoDAO extends IBaseDAO<FotoAuto> {
}
