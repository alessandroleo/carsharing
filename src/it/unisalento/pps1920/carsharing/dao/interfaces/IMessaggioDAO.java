package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.Messaggio;

import java.util.ArrayList;

public interface IMessaggioDAO extends IBaseDAO<Messaggio> {

    public void salvaMessaggio(Messaggio m);

    public ArrayList<Messaggio> findAllByEmail(String email);
}
