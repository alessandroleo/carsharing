package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.Motorizzazione;

public interface IMotorizzazioneDAO extends IBaseDAO<Motorizzazione> {
}
