package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.Mezzo;

import java.util.ArrayList;

public interface IMezzoDAO extends IBaseDAO<Mezzo> {

    public ArrayList<Mezzo> sortByModello();

    public ArrayList<Mezzo> sortByMarca();

    public ArrayList<Mezzo> sortByMotorizzazione();

}
