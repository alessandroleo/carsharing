package it.unisalento.pps1920.carsharing.dao.interfaces;

import it.unisalento.pps1920.carsharing.model.RichiestaCondivisione;

public interface IRichiestaCondivisioneDAO extends IBaseDAO<RichiestaCondivisione> {
}
