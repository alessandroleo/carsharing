package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.IStazioneDAO;
import it.unisalento.pps1920.carsharing.model.Stazione;

import java.util.ArrayList;

public class StazioneDAO implements IStazioneDAO {

    @Override
    public Stazione findById(int id) {
        Stazione s = null;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM stazione WHERE id = " + id + ";");

        if (res.size() == 1) {

            String riga[] = res.get(0);
            s = new Stazione();

            s.setId(Integer.parseInt(riga[0]));
            s.setNome(riga[1]);
        }

        return s;
    }

    @Override
    public ArrayList<Stazione> findAll() {

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM stazione");

        ArrayList<Stazione> stazioni = new ArrayList<Stazione>();

        for (String[] riga : res) {
            Stazione s = new Stazione();

            s.setId(Integer.parseInt(riga[0]));
            s.setNome(riga[1]);

            stazioni.add(s);
        }
        return stazioni;
    }

    @Override
    public void save(Stazione stazione) {
        boolean success = false;
        if (stazione.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE stazione " +

                            "SET " +
                            "nome='" + stazione.getNome() + "' " +

                            "WHERE id='" + stazione.getId() + ";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO stazione (" +

                            "nome," +
                            "latitudine, " +
                            "longitudine " +

                            "VALUES ('" +
                            stazione.getNome() + "');");
        }
    }

    @Override
    public void delete(Stazione stazione) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM stazione WHERE id="+stazione.getId()+";");
    }

}
