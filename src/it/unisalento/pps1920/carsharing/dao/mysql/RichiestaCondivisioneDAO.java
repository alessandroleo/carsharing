package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.*;
import it.unisalento.pps1920.carsharing.model.*;
import it.unisalento.pps1920.carsharing.util.DateUtil;

import java.util.ArrayList;

public class RichiestaCondivisioneDAO implements IRichiestaCondivisioneDAO {

    @Override
    public RichiestaCondivisione findById(int id) {

        RichiestaCondivisione rc = null;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM richiestaCondivisione WHERE id = " + id + ";");

        if (res.size() == 1) {

            String[] riga = res.get(0);
            rc = new RichiestaCondivisione();

            IUtenteDAO cDao = new UtenteDAO();
            IPrenotazioneDAO pDao = new PrenotazioneDAO();

            Utente cliente = cDao.findById(Integer.parseInt(riga[5]));
            Prenotazione prenotazione = pDao.findById(Integer.parseInt(riga[5]));

            rc.setId(Integer.parseInt(riga[0]));
            rc.setData(DateUtil.dateTimeFromString(riga[1]));
            rc.setStato(riga[2]);
            rc.setCliente(cliente);
            rc.setPrenotazione(prenotazione);
        }
        return rc;
    }

    @Override
    public ArrayList<RichiestaCondivisione> findAll() {

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM richiestaCondivisione");

        ArrayList<RichiestaCondivisione> richieste = new ArrayList<RichiestaCondivisione>();

        for(String[] riga : res) {
            RichiestaCondivisione rc = new RichiestaCondivisione();

            IUtenteDAO cDao = new UtenteDAO();
            IPrenotazioneDAO iDao = new PrenotazioneDAO();

            Utente cliente = cDao.findById(Integer.parseInt(riga[4]));
            Prenotazione prenotazione = iDao.findById(Integer.parseInt(riga[5]));

            rc.setId(Integer.parseInt(riga[0]));
            rc.setData(DateUtil.dateTimeFromString(riga[1]));
            rc.setStato(riga[2]);
            rc.setCliente(cliente);
            rc.setPrenotazione(prenotazione);

            richieste.add(rc);
        }
        return richieste;
    }

    @Override
    public void save(RichiestaCondivisione richiestaCondivisione) {
        boolean success = false;
        if (richiestaCondivisione.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE richiestaCondivisione " +
                            "SET " +

                            "data='" + richiestaCondivisione.getData() + "', " +
                            "stato='" + richiestaCondivisione.getStato() + "', " +
                            "cliente_id='" + richiestaCondivisione.getCliente().getId() + "', " +
                            "prenotazione_id='" + richiestaCondivisione.getPrenotazione().getId() + "' " +

                            "WHERE id=" + richiestaCondivisione.getId() + ";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO richiestaCondivisione (" +
                            "data, " +
                            "stato, " +
                            "cliente_id, " +
                            "prenotazione_id)" +

                            "VALUES ('" +
                            richiestaCondivisione.getData() + "', '" +
                            richiestaCondivisione.getStato() + "', '" +
                            richiestaCondivisione.getCliente().getId() + "', '" +
                            richiestaCondivisione.getPrenotazione().getId() + "');");
        }
    }

    @Override
    public void delete(RichiestaCondivisione richiestaCondivisione) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM richiestaCondivisione WHERE id=" + richiestaCondivisione.getId() + ";");
    }
}
