package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.*;
import it.unisalento.pps1920.carsharing.model.*;

import java.util.ArrayList;

public class PrenotazioneDAO implements IPrenotazioneDAO {

    @Override
    public Prenotazione findById(int id) {

        Prenotazione p = null;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM prenotazione WHERE id = "+id+";");

        if(res.size()==1) {

            String[] riga = res.get(0);
            p = new Prenotazione();

            IUtenteDAO uDao = new UtenteDAO();
            IMezzoDAO mDao = new MezzoDAO();
            ILocalitaDAO lDao = new LocalitaDAO();
            IStazioneDAO sDao = new StazioneDAO();

            Utente cliente = uDao.findById(Integer.parseInt(riga[5]));
            Mezzo mezzo = mDao.findById(Integer.parseInt(riga[6]));
            Localita l = lDao.findById(Integer.parseInt(riga[7]));
            Stazione partenza = sDao.findById(Integer.parseInt(riga[8]));
            Stazione arrivo = sDao.findById(Integer.parseInt(riga[9]));

            p.setId(Integer.parseInt(riga[0]));
            p.setData_prenotazione((riga[1]));
            p.setData_inizio((riga[2]));
            p.setData_fine((riga[2]));
            p.setNum_posti_occupati(Integer.parseInt(riga[4]));
            p.setCliente(cliente);
            p.setMezzo(mezzo);
            p.setLocalita(l);
            p.setStazione_partenza(partenza);
            p.setStazione_arrivo(arrivo);
            p.setPrezzoFinale(Integer.parseInt(riga[10]));

            ArrayList<String[]> accessoriRes = DbConnection.getInstance().eseguiQuery("SELECT * FROM prenotazione_accessorio WHERE prenotazione_id = "+p.getId()+";");

            ArrayList<Accessorio> accessori = new ArrayList<>();
            IAccessorioDAO aDao = new AccessorioDAO();
            if(accessoriRes.size() > 0) {
                for(String[] accessorioRiga : accessoriRes) {
                    Accessorio accessorio = aDao.findById(Integer.parseInt(accessorioRiga[1]));
                    accessori.add(accessorio);
                }
            }
            p.setAccessori(accessori);
        }

        return p;

    }

    @Override
    public ArrayList<Prenotazione> findAll() {

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM prenotazione");

        ArrayList<Prenotazione> prenotazioni = new ArrayList<Prenotazione>();

        for(String[] riga : res) {
            Prenotazione p = new Prenotazione();

            IUtenteDAO uDao = new UtenteDAO();
            IMezzoDAO mDao = new MezzoDAO();
            ILocalitaDAO lDao = new LocalitaDAO();
            IStazioneDAO sDao = new StazioneDAO();

            Utente cliente = uDao.findById(Integer.parseInt(riga[6]));
            Mezzo mezzo = mDao.findById(Integer.parseInt(riga[7]));
            Localita l = lDao.findById(Integer.parseInt(riga[8]));
            Stazione partenza = sDao.findById(Integer.parseInt(riga[9]));
            Stazione arrivo = sDao.findById(Integer.parseInt(riga[10]));

            p.setId(Integer.parseInt(riga[0]));
            p.setData_prenotazione((riga[1]));
            p.setData_inizio((riga[2]));
            p.setData_fine((riga[3]));
            p.setNum_posti_occupati(Integer.parseInt(riga[4]));
            p.setPrezzoFinale(Integer.parseInt(riga[5]));
            p.setCliente(cliente);
            p.setMezzo(mezzo);
            p.setLocalita(l);
            p.setStazione_partenza(partenza);
            p.setStazione_arrivo(arrivo);
            p.setPagato(Integer.parseInt(riga[11]));
            p.setAccessoriato(Integer.parseInt(riga[12]));


            ArrayList<String[]> accessoriRes = DbConnection.getInstance().eseguiQuery("SELECT * FROM prenotazione_accessorio WHERE prenotazione_id = "+p.getId()+";");

            ArrayList<Accessorio> accessori = new ArrayList<>();
            IAccessorioDAO aDao = new AccessorioDAO();
            if(accessoriRes.size() > 0) {
                for(String[] accessorioRiga : accessoriRes) {
                    Accessorio accessorio = aDao.findById(Integer.parseInt(accessorioRiga[1]));
                    accessori.add(accessorio);
                }
            }
            p.setAccessori(accessori);

            prenotazioni.add(p);
        }
        return prenotazioni;
    }

    @Override
    public ArrayList<Prenotazione> findAllByClienteId(int clienteId) {

        String query = "SELECT P.id, " +
                "   P.data_prenotazione, " +
                "   P.data_inizio, " +
                "   P.data_fine, " +
                "   P.num_posti_occupati, " +
                "   P.prezzo_finale, " +
                "   U.id as cliente_id, " +
                "   U.nome as cliente_nome, " +
                "   U.cognome as cliente_cognome, " +
                "   M.id as mezzo_id, " +
                "   M.targa as mezzo_targa, " +
                "   Mo.id as mezzo_modello_id, " +
                "   Mo.nome as mezzo_modello_nome, " +
                "   Mo.numero_posti as mezzo_modello_numPostiOccupati, " +
                "   Mar.id as mezzo_marca_id," +
                "   Mar.nome as mezzo_marca_nome, " +
                "   Mot.id as mezzo_motorizzazione_id, " +
                "   Mot.tipo as mezzo_motorizzazione_nome, " +
                "   L.id as localita_id, " +
                "   L.citta as localita_citta, " +
                "   S1.id as stazione_partenza_id, " +
                "   S1.nome as stazione_partenza_nome, " +
                "   S2.id as stazione_arrivo_id, " +
                "   S2.nome as stazione_arrivo_nome " +
                "FROM prenotazione as P INNER JOIN prenotazione_cliente as PC ON P.id = PC.prenotazione_id " +
                "INNER JOIN utente as U ON PC.cliente_id = U.id " +
                "INNER JOIN mezzo as M ON P.mezzo_id = M.id " +
                "INNER JOIN localita as L on P.localita_id = L.id " +
                "INNER JOIN stazione as S1 on P.stazione_partenza_id = S1.id " +
                "INNER JOIN stazione as S2 on P.stazione_arrivo_id = S2.id " +
                "INNER JOIN modello as Mo on M.modello_id = Mo.id " +
                "INNER JOIN marca as Mar on M.marca_id = Mar.id " +
                "INNER JOIN motorizzazione as Mot on M.motorizzazione_id = Mot.id " +

                "WHERE PC.cliente_id = " + clienteId+";";

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery(query);

        ArrayList<Prenotazione> prenotazioni = new ArrayList<Prenotazione>();

        for(String[] riga : res) {
            Prenotazione p = new Prenotazione();

            // Cliente
            Utente cliente = new Utente();
            cliente.setId(Integer.parseInt(riga[6]));
            cliente.setNome(riga[7]);
            cliente.setCognome(riga[8]);

            // Mezzo
            Mezzo mezzo = new Mezzo();
            mezzo.setId(Integer.parseInt(riga[9]));
            mezzo.setTarga(riga[10]);
            Modello modello = new Modello();
            modello.setId(Integer.parseInt(riga[11]));
            modello.setNome(riga[12]);
            modello.setNumero_posti(Integer.parseInt(riga[13]));
            mezzo.setModello(modello);
            Marca marca = new Marca();
            marca.setId(Integer.parseInt(riga[14]));
            marca.setNome(riga[15]);
            mezzo.setMarca(marca);
            Motorizzazione motorizzazione = new Motorizzazione();
            motorizzazione.setId(Integer.parseInt(riga[16]));
            motorizzazione.setTipo(riga[17]);
            mezzo.setMotorizzazione(motorizzazione);

            // Localita
            Localita localita = new Localita();
            localita.setId(Integer.parseInt(riga[18]));
            localita.setCitta(riga[19]);

            // Stazione partenza
            Stazione stazionePartenza = new Stazione();
            stazionePartenza.setId(Integer.parseInt(riga[20]));
            stazionePartenza.setNome(riga[21]);

            // Stazione arrivo
            Stazione stazioneArrivo = new Stazione();
            stazioneArrivo.setId(Integer.parseInt(riga[22]));
            stazioneArrivo.setNome(riga[23]);

            p.setId(Integer.parseInt(riga[0]));
            p.setData_prenotazione((riga[1]));
            p.setData_inizio((riga[2]));
            p.setData_fine((riga[3]));
            p.setNum_posti_occupati(Integer.parseInt(riga[4]));
            p.setPrezzoFinale((riga[5] != null ? Integer.parseInt(riga[5]) : 0));
            p.setCliente(cliente);
            p.setMezzo(mezzo);
            p.setLocalita(localita);
            p.setStazione_partenza(stazionePartenza);
            p.setStazione_arrivo(stazioneArrivo);

            ArrayList<String[]> accessoriRes = DbConnection.getInstance().eseguiQuery("SELECT * FROM prenotazione_accessorio WHERE prenotazione_id = "+p.getId()+";");

            ArrayList<Accessorio> accessori = new ArrayList<>();
            IAccessorioDAO aDao = new AccessorioDAO();
            if(accessoriRes.size() > 0) {
                for(String[] accessorioRiga : accessoriRes) {
                    Accessorio accessorio = aDao.findById(Integer.parseInt(accessorioRiga[1]));
                    accessori.add(accessorio);
                }
            }
            p.setAccessori(accessori);

            prenotazioni.add(p);
        }
        return prenotazioni;
    }

    @Override
    public ArrayList<Prenotazione> sortByData() {
        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM prenotazione ORDER BY data_prenotazione");

        ArrayList<Prenotazione> prenotazioni = new ArrayList<Prenotazione>();

        for(String[] riga : res) {
            Prenotazione p = new Prenotazione();

            IUtenteDAO uDao = new UtenteDAO();
            IMezzoDAO mDao = new MezzoDAO();
            ILocalitaDAO lDao = new LocalitaDAO();
            IStazioneDAO sDao = new StazioneDAO();

            Utente cliente = uDao.findById(Integer.parseInt(riga[6]));
            Mezzo mezzo = mDao.findById(Integer.parseInt(riga[7]));
            Localita l = lDao.findById(Integer.parseInt(riga[8]));
            Stazione partenza = sDao.findById(Integer.parseInt(riga[9]));
            Stazione arrivo = sDao.findById(Integer.parseInt(riga[10]));

            p.setId(Integer.parseInt(riga[0]));
            p.setData_prenotazione((riga[1]));
            p.setData_inizio((riga[2]));
            p.setData_fine((riga[3]));
            p.setNum_posti_occupati(Integer.parseInt(riga[4]));
            p.setPrezzoFinale(Integer.parseInt(riga[5]));
            p.setCliente(cliente);
            p.setMezzo(mezzo);
            p.setLocalita(l);
            p.setStazione_partenza(partenza);
            p.setStazione_arrivo(arrivo);
            p.setPagato(Integer.parseInt(riga[11]));
            p.setAccessoriato(Integer.parseInt(riga[12]));

            prenotazioni.add(p);
        }
        return prenotazioni;
    }

    @Override
    public ArrayList<Prenotazione> sortByStazione() {
        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM prenotazione ORDER BY stazione_partenza_id");

        ArrayList<Prenotazione> prenotazioni = new ArrayList<Prenotazione>();

        for(String[] riga : res) {
            Prenotazione p = new Prenotazione();

            IUtenteDAO uDao = new UtenteDAO();
            IMezzoDAO mDao = new MezzoDAO();
            ILocalitaDAO lDao = new LocalitaDAO();
            IStazioneDAO sDao = new StazioneDAO();

            Utente cliente = uDao.findById(Integer.parseInt(riga[6]));
            Mezzo mezzo = mDao.findById(Integer.parseInt(riga[7]));
            Localita l = lDao.findById(Integer.parseInt(riga[8]));
            Stazione partenza = sDao.findById(Integer.parseInt(riga[9]));
            Stazione arrivo = sDao.findById(Integer.parseInt(riga[10]));

            p.setId(Integer.parseInt(riga[0]));
            p.setData_prenotazione((riga[1]));
            p.setData_inizio((riga[2]));
            p.setData_fine((riga[3]));
            p.setNum_posti_occupati(Integer.parseInt(riga[4]));
            p.setPrezzoFinale(Integer.parseInt(riga[5]));
            p.setCliente(cliente);
            p.setMezzo(mezzo);
            p.setLocalita(l);
            p.setStazione_partenza(partenza);
            p.setStazione_arrivo(arrivo);
            p.setPagato(Integer.parseInt(riga[11]));
            p.setAccessoriato(Integer.parseInt(riga[12]));

            prenotazioni.add(p);
        }
        return prenotazioni;
    }

    @Override
    public ArrayList<Prenotazione> sortByMezzo() {

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM prenotazione ORDER BY mezzo_id");

        ArrayList<Prenotazione> prenotazioni = new ArrayList<Prenotazione>();

        for(String[] riga : res) {
            Prenotazione p = new Prenotazione();

            IUtenteDAO uDao = new UtenteDAO();
            IMezzoDAO mDao = new MezzoDAO();
            ILocalitaDAO lDao = new LocalitaDAO();
            IStazioneDAO sDao = new StazioneDAO();

            Utente cliente = uDao.findById(Integer.parseInt(riga[6]));
            Mezzo mezzo = mDao.findById(Integer.parseInt(riga[7]));
            Localita l = lDao.findById(Integer.parseInt(riga[8]));
            Stazione partenza = sDao.findById(Integer.parseInt(riga[9]));
            Stazione arrivo = sDao.findById(Integer.parseInt(riga[10]));

            p.setId(Integer.parseInt(riga[0]));
            p.setData_prenotazione((riga[1]));
            p.setData_inizio((riga[2]));
            p.setData_fine((riga[3]));
            p.setNum_posti_occupati(Integer.parseInt(riga[4]));
            p.setPrezzoFinale(Integer.parseInt(riga[5]));
            p.setCliente(cliente);
            p.setMezzo(mezzo);
            p.setLocalita(l);
            p.setStazione_partenza(partenza);
            p.setStazione_arrivo(arrivo);
            p.setPagato(Integer.parseInt(riga[11]));
            p.setAccessoriato(Integer.parseInt(riga[12]));

            prenotazioni.add(p);
        }
        return prenotazioni;
    }

    @Override
    public ArrayList<Prenotazione> findAllByStazioneId(int stazione_id) {
        String query = "SELECT P.id, " +
                "   P.data_prenotazione, " +
                "   P.data_inizio, " +
                "   P.data_fine, " +
                "   P.num_posti_occupati, " +
                "   P.prezzo_finale, " +
                "   P.pagato, " +
                "   P.accessoriato, " +
                "   U.id as cliente_id, " +
                "   U.nome as cliente_nome, " +
                "   U.cognome as cliente_cognome, " +
                "   M.id as mezzo_id, " +
                "   M.targa as mezzo_targa, " +
                "   Mo.id as mezzo_modello_id, " +
                "   Mo.nome as mezzo_modello_nome, " +
                "   Mo.numero_posti as mezzo_modello_numPostiOccupati, " +
                "   Mar.id as mezzo_marca_id," +
                "   Mar.nome as mezzo_marca_nome, " +
                "   Mot.id as mezzo_motorizzazione_id, " +
                "   Mot.tipo as mezzo_motorizzazione_nome, " +
                "   L.id as localita_id, " +
                "   L.citta as localita_citta, " +
                "   S1.id as stazione_partenza_id, " +
                "   S1.nome as stazione_partenza_nome, " +
                "   S2.id as stazione_arrivo_id, " +
                "   S2.nome as stazione_arrivo_nome " +
                "FROM prenotazione as P " +
                "INNER JOIN utente as U ON P.cliente_id = U.id " +
                "INNER JOIN mezzo as M ON P.mezzo_id = M.id " +
                "INNER JOIN localita as L on P.localita_id = L.id " +
                "INNER JOIN stazione as S1 on P.stazione_partenza_id = S1.id " +
                "INNER JOIN stazione as S2 on P.stazione_arrivo_id = S2.id " +
                "INNER JOIN modello as Mo on M.modello_id = Mo.id " +
                "INNER JOIN marca as Mar on M.marca_id = Mar.id " +
                "INNER JOIN motorizzazione as Mot on M.motorizzazione_id = Mot.id " +

                "WHERE P.stazione_partenza_id = " + stazione_id + ";";

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery(query);

        ArrayList<Prenotazione> prenotazioni = new ArrayList<Prenotazione>();

        for(String[] riga : res) {

            Prenotazione p = new Prenotazione();

            Utente cliente = new Utente();
            cliente.setId(Integer.parseInt(riga[8]));
            cliente.setNome(riga[9]);
            cliente.setCognome(riga[10]);

            // Mezzo
            Mezzo mezzo = new Mezzo();
            mezzo.setId(Integer.parseInt(riga[11]));
            mezzo.setTarga(riga[12]);
            Modello modello = new Modello();
            modello.setId(Integer.parseInt(riga[13]));
            modello.setNome(riga[14]);
            modello.setNumero_posti(Integer.parseInt(riga[15]));
            mezzo.setModello(modello);
            Marca marca = new Marca();
            marca.setId(Integer.parseInt(riga[16]));
            marca.setNome(riga[17]);
            mezzo.setMarca(marca);
            Motorizzazione motorizzazione = new Motorizzazione();
            motorizzazione.setId(Integer.parseInt(riga[18]));
            motorizzazione.setTipo(riga[19]);
            mezzo.setMotorizzazione(motorizzazione);

            // Localita
            Localita localita = new Localita();
            localita.setId(Integer.parseInt(riga[20]));
            localita.setCitta(riga[21]);

            // Stazione partenza
            Stazione stazionePartenza = new Stazione();
            stazionePartenza.setId(Integer.parseInt(riga[22]));
            stazionePartenza.setNome(riga[23]);

            // Stazione arrivo
            Stazione stazioneArrivo = new Stazione();
            stazioneArrivo.setId(Integer.parseInt(riga[24]));
            stazioneArrivo.setNome(riga[25]);

            p.setId(Integer.parseInt(riga[0]));
            p.setData_prenotazione((riga[1]));
            p.setData_inizio((riga[2]));
            p.setData_fine((riga[3]));
            p.setNum_posti_occupati(Integer.parseInt(riga[4]));
            p.setCliente(cliente);
            p.setMezzo(mezzo);
            p.setLocalita(localita);
            p.setStazione_partenza(stazionePartenza);
            p.setStazione_arrivo(stazioneArrivo);
            p.setPrezzoFinale(Integer.parseInt(riga[5]));
            p.setPagato(Integer.parseInt(riga[6]));
            p.setAccessoriato(Integer.parseInt(riga[7]));

            ArrayList<String[]> accessoriRes = DbConnection.getInstance().eseguiQuery("SELECT * FROM prenotazione_accessorio WHERE prenotazione_id = "+p.getId()+";");

            ArrayList<Accessorio> accessori = new ArrayList<>();
            IAccessorioDAO aDao = new AccessorioDAO();
            if(accessoriRes.size() > 0) {
                for(String[] accessorioRiga : accessoriRes) {
                    Accessorio accessorio = aDao.findById(Integer.parseInt(accessorioRiga[1]));
                    accessori.add(accessorio);
                }
            }
            p.setAccessori(accessori);

            prenotazioni.add(p);
        }
        return prenotazioni;
    }

    @Override
    public void save(Prenotazione prenotazione) {
        boolean success = false;
        if (prenotazione.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE prenotazione " +
                            "SET " +

                            "data_prenotazione='" + prenotazione.getData_prenotazione() + "', " +
                            "data_inizio='" + prenotazione.getData_inizio() + "', " +
                            "data_fine='" + prenotazione.getData_fine() + "', " +
                            "num_posti_occupati='" + prenotazione.getNum_posti_occupati() + "', " +
                            "prezzo_finale='" + prenotazione.getPrezzoFinale() + "', " +
                            "cliente_id='" + prenotazione.getCliente().getId() + "', " +
                            "mezzo_id='" + prenotazione.getMezzo().getId() + "', " +
                            "localita_id='" + prenotazione.getLocalita().getId() + "', " +
                            "stazione_partenza_id='" + prenotazione.getStazione_partenza().getId() + "', " +
                            "stazione_arrivo_id='" + prenotazione.getStazione_arrivo().getId() + "', " +
                            "pagato='" + prenotazione.getPagato() + "', " +
                            "accessoriato='" +prenotazione.getAccessoriato() + "', " +
                            "full='" + prenotazione.getFull() + "', " +
                            "sharable='" + prenotazione.getSharable() + "' " +

                            "WHERE id=" + prenotazione.getId() + ";");

            DbConnection.getInstance().eseguiAggiornamento("DELETE FROM prenotazione_accessorio WHERE prenotazione_id=" + prenotazione.getId());


            for (int i = 0; i < prenotazione.getAccessori().size(); i++) {
                int accessorioId = prenotazione.getAccessori().get(i).getId();
                DbConnection.getInstance().eseguiAggiornamento(
                        "INSERT INTO prenotazione_accessorio (" +
                                "prenotazione_id, " +
                                "accessorio_id) " +
                                "VALUES (" +
                                prenotazione.getId() + ", " +
                                accessorioId + ");");

            }

        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO prenotazione (" +
                            "data_prenotazione, " +
                            "data_inizio, "  +
                            "data_fine, " +
                            "num_posti_occupati, "  +
                            "prezzo_finale, " +
                            "cliente_id, " +
                            "mezzo_id, " +
                            "localita_id, " +
                            "stazione_partenza_id, " +
                            "stazione_arrivo_id, " +
                            "pagato, " +
                            "accessoriato, " +
                            "full, " +
                            "sharable) " +

                            "VALUES ('" +
                            prenotazione.getData_prenotazione() + "', '" +
                            prenotazione.getData_inizio() + "', '" +
                            prenotazione.getData_fine() + "', '" +
                            prenotazione.getNum_posti_occupati() + "', '" +
                            prenotazione.getPrezzoFinale() + "', '" +
                            prenotazione.getCliente().getId() + "', '" +
                            prenotazione.getMezzo().getId() + "', '" +
                            prenotazione.getLocalita().getId() + "', '" +
                            prenotazione.getStazione_partenza().getId() + "', '" +
                            prenotazione.getStazione_arrivo().getId() + "', '" +
                            prenotazione.getPagato() + "', '" +
                            prenotazione.getAccessoriato() + "', '" +
                            prenotazione.getFull() + "', '" +
                            prenotazione.getSharable() + "');");

            int prenotazioneId = Integer.parseInt(DbConnection.getInstance().eseguiQuery("SELECT last_insert_id()").get(0)[0]);

            for (int i = 0; i < prenotazione.getAccessori().size(); i++) {
                int accessorioId = prenotazione.getAccessori().get(i).getId();
                DbConnection.getInstance().eseguiAggiornamento(
                        "INSERT INTO prenotazione_accessorio (" +
                                "prenotazione_id, " +
                                "accessorio_id) " +

                                "VALUES (" +
                                prenotazioneId + ", " +
                                accessorioId + ");");


            }


                int clienteId = prenotazione.getCliente().getId();
                DbConnection.getInstance().eseguiAggiornamento(
                        "INSERT INTO prenotazione_cliente (" +
                                "prenotazione_id, " +
                                "cliente_id) " +

                                "VALUES (" +
                                prenotazioneId + ", " +
                                clienteId + ");");

            }
    }

    @Override
    public void delete(Prenotazione prenotazione) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento(
                "DELETE FROM prenotazione WHERE id=" + prenotazione.getId()+";");
    }

    @Override
    public int checkPrenotazione(Prenotazione p) {
        String query = "SELECT * FROM prenotazione" +
                        " WHERE data_inizio = '" + p.getData_inizio() + "'" +
                        " AND data_fine = '" + p.getData_inizio() + "'" +
                        " AND localita_id = " + p.getLocalita().getId() +
                        " AND mezzo_id = " + p.getMezzo().getId() +
                        " AND stazione_partenza_id = " + p.getStazione_partenza().getId() +
                        " AND stazione_arrivo_id = " + p.getStazione_arrivo().getId() +
                        " AND num_posti_occupati != " + 0 +
                        " AND full = " + 0 +
                        " AND sharable = " + 1;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery(query);

        if(res.size() > 0) {

            int idPrenotazione = Integer.parseInt(res.get(res.size() -1)[0]);

            return idPrenotazione;

        } else {
            return 0;
        }
    }

    @Override
    public ArrayList<Utente> findAllClientiByPrenotazioneId(int prenotazione_id) {

        ArrayList<String[]> risultato = DbConnection.getInstance().eseguiQuery(
                "SELECT U.id, " +
                "U.nome, " +
                "U.cognome, " +
                "U.email, " +
                "U.telefono, " +
                "U.residenza, " +
                "U.eta, " +
                "U.foto, " +
                "PC.prenotazione_id as prenotazione_id, " +
                "PC.cliente_id as cliente_id " +
                "FROM utente as U " +
                "INNER JOIN prenotazione_cliente as PC on U.id = PC.cliente_id " +
                "WHERE prenotazione_id = " + prenotazione_id + ";");

        ArrayList<Utente> utenti = new ArrayList<Utente>();

        for(String[] riga : risultato) {
            Utente u = new Utente();

            u.setId(Integer.parseInt(riga[0]));
            u.setNome(riga[1]);
            u.setCognome(riga[2]);
            u.setEmail(riga[3]);
            u.setTelefono(riga[4]);
            u.setResidenza(riga[5]);
            u.setEta(Integer.parseInt(riga[6]));
            byte[] foto = DbConnection.getInstance().getFoto("SELECT foto FROM utente WHERE id = " + riga[0] + ";");
            u.setFoto(foto);

            utenti.add(u);
        }

        return utenti;
    }

    @Override
    public void addNewCliente(int clienteId, int prenotazioneId) {
        DbConnection.getInstance().eseguiAggiornamento(
                "INSERT INTO prenotazione_cliente (" +
                        "prenotazione_id, " +
                        "cliente_id) " +

                        "VALUES (" +
                        prenotazioneId + ", " +
                        clienteId + ");");

        DbConnection.getInstance().eseguiAggiornamento(
                "UPDATE prenotazione " +
                        "SET " +

                        "num_posti_occupati = num_posti_occupati + " + 1 + " " +

                        "WHERE id=" + prenotazioneId + ";");

        String query = "SELECT MO.numero_posti, " +
                        "P.num_posti_occupati as prenotazione_num_posti_occupati, " +
                        "P.mezzo_id as prenotazione_mezzo_id " +

                        "FROM mezzo as ME INNER JOIN modello as MO ON ME.modello_id = MO.id " +
                        "INNER JOIN prenotazione as P ON P.mezzo_id = ME.id " +

                        "WHERE P.id = " + prenotazioneId + ";";

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery(query);
        int nomeroTotalePostiMezzoModello = Integer.parseInt(res.get(0)[0]);
        int prenotazioneNumPostiOccupati = Integer.parseInt(res.get(0)[1]);
        if ((prenotazioneNumPostiOccupati) == nomeroTotalePostiMezzoModello){
            DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE prenotazione " +
                            "SET " +

                            "full = " + 1 + " " +

                            "WHERE id=" + prenotazioneId + ";");
        }
    }

    @Override
    public void deleteByClienteId(int clienteId, int prenotazioneId) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento(
                "DELETE FROM prenotazione_cliente WHERE cliente_id =" + clienteId+";");

        DbConnection.getInstance().eseguiAggiornamento(
                "UPDATE prenotazione " +
                        "SET " +

                        "num_posti_occupati = num_posti_occupati + " + (-1) + " " +

                        "WHERE id=" + prenotazioneId + ";");
    }
}
