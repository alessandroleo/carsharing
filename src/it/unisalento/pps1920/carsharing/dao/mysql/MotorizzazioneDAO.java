package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.IMotorizzazioneDAO;
import it.unisalento.pps1920.carsharing.model.Motorizzazione;

import java.util.ArrayList;

public class MotorizzazioneDAO implements IMotorizzazioneDAO {

    @Override
    public Motorizzazione findById(int id) {
        Motorizzazione mo = null;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM motorizzazione WHERE id = "+id+";");

        if(res.size() == 1) {

            mo= new Motorizzazione();
            String[] riga = res.get(0);

            mo.setId(Integer.parseInt(riga[0]));
            mo.setTipo(riga[1]);
        }
        return mo;
    }

    @Override
    public ArrayList<Motorizzazione> findAll() {
        ArrayList<String[]> risultato = DbConnection.getInstance().eseguiQuery("SELECT * FROM motorizzazione;");

        ArrayList<Motorizzazione> motorizzationi = new ArrayList<Motorizzazione>();

        for(String[] riga : risultato) {
            Motorizzazione mo = new Motorizzazione();

            mo.setId(Integer.parseInt(riga[0]));
            mo.setTipo(riga[1]);

            motorizzationi.add(mo);
        }

        return motorizzationi;
    }

    @Override
    public void save(Motorizzazione motorizzazione) {
        boolean success = false;
        if (motorizzazione.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE motorizzazione " +
                            "SET " +

                            "nome='" + motorizzazione.getTipo() + "' " +

                            "WHERE id=" + motorizzazione.getId() + ";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO marca (" +
                            "nome, " +

                            "VALUES ('" +
                            motorizzazione.getTipo() + "');");

        }
    }

    @Override
    public void delete(Motorizzazione motorizzazione) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM motorizzazione WHERE id=" + motorizzazione.getId() + ";");
    }
}
