package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.IMessaggioDAO;
import it.unisalento.pps1920.carsharing.model.Messaggio;

import java.util.ArrayList;

public class MessaggioDAO implements IMessaggioDAO {
    /*
    @Override
    public MessaggioDAO findById(String email) {
        Messaggio m = null;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM messaggio WHERE destinatario="+ email +";");

        if(res.size()==1) {

            String[] riga = res.get(0);
            m = new Messaggio();
            m.setId(Integer.parseInt(riga[0]));
           m.setmessaggio(riga[1]);
            m.setmittente(riga[2]);
            m.setdestinatario(riga[3]);

        }
        return m;
    }
*/
    @Override
    public ArrayList<Messaggio> findAllByEmail(String email) {
        ArrayList<Messaggio> messaggi = new ArrayList<Messaggio>();

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM messaggio WHERE destinatario='"+email+"';");

        for(String[] riga : res) {
            Messaggio m = new Messaggio();

            m.setId(Integer.parseInt(riga[0]));
            m.setMessaggio(riga[1]);
            m.setMittente(riga[2]);
            m.setDestinatario(riga[3]);
            messaggi.add(m);
        }
        return messaggi;
    }


    @Override
    public Messaggio findById(int id) {
        return null;
    }

    @Override
    public ArrayList<Messaggio> findAll() {
        ArrayList<String[]> risultato = DbConnection.getInstance().eseguiQuery("SELECT * FROM messaggio;");

        ArrayList<Messaggio> messaggi = new ArrayList<Messaggio>();

        for(String[] riga : risultato) {
            Messaggio m = new Messaggio();

            m.setId(Integer.parseInt(riga[0]));
            m.setMessaggio(riga[1]);
            m.setMittente(riga[2]);
            m.setDestinatario(riga[3]);
            messaggi.add(m);
        }

        return messaggi;
    }


    @Override
    public void save(Messaggio messaggio) {
        boolean success = false;
        if (messaggio.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE messaggio " +
                            "SET " +

                            "messaggio='"+messaggio.getMessaggio()+ "', " +
                            "mittente='"+messaggio.getMittente()+ "', " +
                            "destinatario='"+messaggio.getDestinatario()+ "', " +


                            "WHERE id="+messaggio.getId()+";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO messaggio (" +
                            "messaggio," +
                            "mittente, " +
                            "destinatario) " +
                            "VALUES ("+
                            "'" +messaggio.getMessaggio()+ "', " +
                            "'" + messaggio.getMittente()+ "', " +
                            "'" + messaggio.getDestinatario()+ "');");
        }
    }

    @Override
    public void delete(Messaggio messaggio) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM messaggio WHERE id="+messaggio.getId()+";");
    }

    @Override
    public void salvaMessaggio(Messaggio m) {

    }


}
