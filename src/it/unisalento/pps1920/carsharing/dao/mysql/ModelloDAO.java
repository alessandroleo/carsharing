package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.IModelloDAO;
import it.unisalento.pps1920.carsharing.model.Modello;

import java.util.ArrayList;

public class ModelloDAO implements IModelloDAO {

    @Override
    public Modello findById(int id) {

        Modello m = null;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM modello WHERE id = "+id+";");

        if(res.size() == 1) {

            m= new Modello();
            String[] riga = res.get(0);

            m.setId(Integer.parseInt(riga[0]));
            m.setNome(riga[1]);
            m.setNumero_posti(Integer.parseInt(riga[2]));

        }
        return m;
    }

    @Override
    public ArrayList<Modello> findAll() {

        ArrayList<String[]> risultato = DbConnection.getInstance().eseguiQuery("SELECT * FROM modello;");

        ArrayList<Modello> modelli = new ArrayList<Modello>();

        for(String[] riga : risultato) {
            Modello m = new Modello();

            m.setId(Integer.parseInt(riga[0]));
            m.setNome(riga[1]);
            m.setNumero_posti(Integer.parseInt(riga[2]));

            modelli.add(m);
        }

        return modelli;

    }

    @Override
    public void save(Modello modello) {
        boolean success = false;
        if (modello.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE modello " +
                            "SET " +

                            "nome='" + modello.getNome() + "'," +
                            "numero_posti='" + modello.getNumero_posti() + "' " +

                            "WHERE id=" + modello.getId() + ";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO modello (" +
                            "nome, " +
                            "numero_posti) " +

                            "VALUES ('" +
                            modello.getNome() + "', '" +
                            modello.getNumero_posti() + "');");
        }
    }

    @Override
    public void delete(Modello modello) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM modello WHERE id=" + modello.getId() + ";");
    }
}
