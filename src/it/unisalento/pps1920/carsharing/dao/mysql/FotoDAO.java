package it.unisalento.pps1920.carsharing.dao.mysql;


import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.IFotoDAO;
import it.unisalento.pps1920.carsharing.dao.interfaces.IMarcaDAO;
import it.unisalento.pps1920.carsharing.dao.interfaces.IModelloDAO;
import it.unisalento.pps1920.carsharing.model.*;

import java.util.ArrayList;

public class FotoDAO implements IFotoDAO {
    @Override
    public FotoAuto findById(int id) {
        FotoAuto fm = null;

        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM foto_auto WHERE id = " + id + ";");

        if(result.size() == 1) {

            fm = new FotoAuto();
            String riga[] = result.get(0);

            IModelloDAO ModDao = new ModelloDAO();
            IMarcaDAO MarDao = new MarcaDAO();

            Modello modello = ModDao.findById(Integer.parseInt(riga[1]));
            Marca marca = MarDao.findById(Integer.parseInt(riga[2]));

            fm.setId(Integer.parseInt(riga[0]));
            fm.setModello(modello);
            fm.setMarca(marca);
            byte[] foto_auto = DbConnection.getInstance().getFoto("SELECT foto FROM foto_auto WHERE id = "+id+";");
            fm.setFoto(foto_auto);

        }
        return fm;
    }

    @Override
    public ArrayList<FotoAuto> findAll() {
        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM foto_auto;");

        ArrayList<FotoAuto> fotoArrayList = new ArrayList<FotoAuto>();

        for (int i = 0; i < result.size(); i++ ) {
            FotoAuto fm = new FotoAuto();

            String riga[] = result.get(0);

            IModelloDAO ModDao = new ModelloDAO();
            IMarcaDAO MarDao = new MarcaDAO();

            Modello modello = ModDao.findById(Integer.parseInt(riga[1]));
            Marca marca = MarDao.findById(Integer.parseInt(riga[2]));

            fm.setId(Integer.parseInt(riga[0]));
            fm.setModello(modello);
            fm.setMarca(marca);
            byte[] foto_auto = DbConnection.getInstance().getFoto("SELECT foto FROM foto_auto");
            fm.setFoto(foto_auto);

            fotoArrayList.add(fm);
        }
        return fotoArrayList;
    }

    @Override
    public void save(FotoAuto foto_auto) {
        boolean success = false;
        if (foto_auto.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE foto_auto " +
                            "SET " +

                            "modello_id='" + foto_auto.getModello().getId() + "'," +
                            "marca_id='" + foto_auto.getMarca().getId() + "'," +
                            "foto='" + foto_auto.getFoto() + "'," +

                            "WHERE id=" + foto_auto.getId() + ";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO foto_auto (" +
                            "modello_id, " +
                            "marca_id, " +
                            "foto)" +

                            "VALUES ('" +
                            foto_auto.getModello().getId() + "', '" +
                            foto_auto.getMarca().getId() + "', '" +
                            foto_auto.getFoto() + "');");
        }
    }

    @Override
    public void delete(FotoAuto foto_auto) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM foto_auto WHERE id=" + foto_auto.getId() + ";");
    }
}
