package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.IMarcaDAO;
import it.unisalento.pps1920.carsharing.model.Marca;


import java.util.ArrayList;

public class MarcaDAO implements IMarcaDAO {

    @Override
    public Marca findById(int id) {
        Marca ma = null;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM marca WHERE id = "+id+";");

        if(res.size() == 1) {

            ma= new Marca();
            String[] riga = res.get(0);

            ma.setId(Integer.parseInt(riga[0]));
            ma.setNome(riga[1]);
    }
        return ma;
    }

    @Override
    public ArrayList<Marca> findAll() {
        ArrayList<String[]> risultato = DbConnection.getInstance().eseguiQuery("SELECT * FROM marca;");

        ArrayList<Marca> marche = new ArrayList<Marca>();

        for(String[] riga : risultato) {
            Marca ma = new Marca();

            ma.setId(Integer.parseInt(riga[0]));
            ma.setNome(riga[1]);

            marche.add(ma);
        }
        return marche;
    }

    @Override
    public void save(Marca marca) {
        boolean success = false;
        if (marca.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE marca " +
                            "SET " +

                            "nome='" + marca.getNome() + "'," +

                            "WHERE id=" + marca.getId() + ";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO marca (" +
                            "nome, " +

                            "VALUES ('" +
                            marca.getNome() + "');");

        }
    }

    @Override
    public void delete(Marca marca) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM marca WHERE id=" + marca.getId() + ";");
    }
}

