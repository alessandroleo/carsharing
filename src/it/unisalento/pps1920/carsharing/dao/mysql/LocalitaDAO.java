package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.ILocalitaDAO;
import it.unisalento.pps1920.carsharing.model.Localita;

import java.util.ArrayList;

public class LocalitaDAO implements ILocalitaDAO {

    // CRUD (Create, Read, Update, Delete)

    /***************************************************
     * READ
     ***************************************************/

    @Override
    public Localita findById(int id) {
        Localita loc;

        ArrayList<String[]> risultato = DbConnection.getInstance().eseguiQuery("SELECT * FROM localita WHERE id=" + id + ";");

        if(risultato.size()==1) {
            loc = new Localita();
            String[] riga = risultato.get(0);

            loc.setId(Integer.parseInt(riga[0]));
            loc.setCitta(riga[1]);
            loc.setLatitudine(Double.parseDouble(riga[2]));
            loc.setLongitudine(Double.parseDouble(riga[3]));

        }
        else {
            //LOCALITA NON TROVATA
            loc = null;
        }

        return loc;
    }

    @Override
    public ArrayList<Localita> findAll() {

        ArrayList<String[]> risultato = DbConnection.getInstance().eseguiQuery("SELECT * FROM localita;");

        ArrayList<Localita> locs = new ArrayList<Localita>();

        for(String[] riga : risultato) {
            Localita l = new Localita();

            l.setId(Integer.parseInt(riga[0]));
            l.setCitta(riga[1]);
            l.setLatitudine(Double.parseDouble(riga[2]));
            l.setLongitudine(Double.parseDouble(riga[3]));
            locs.add(l);
        }

        return locs;

    }

    /***************************************************
     * CREATE - UPDATE
     ***************************************************/

    @Override
    public void save(Localita localita) {
        boolean success = false;
        if (localita.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE localita " +
                            "SET " +

                            "citta='"+localita.getCitta()+"', " +
                            "latitudine='"+localita.getLatitudine() +"', " +
                            "longitudine='"+localita.getLongitudine() +"' " +

                            "WHERE id="+localita.getId()+";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO localita (" +
                            "citta, " +
                            "latitudine, " +
                            "longitudine) " +

                            "VALUES ('"+
                            localita.getCitta()+"', '"+
                            localita.getLatitudine()+"', '"+
                            localita.getLongitudine()+ "');");
        }
    }

    /***************************************************
     * DELETE
     ***************************************************/

    @Override
    public void delete(Localita localita) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM localita WHERE id="+localita.getId()+";");
    }


}
