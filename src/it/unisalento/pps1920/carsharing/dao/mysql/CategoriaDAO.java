package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.ICategoriaDAO;
import it.unisalento.pps1920.carsharing.dao.interfaces.IModelloDAO;
import it.unisalento.pps1920.carsharing.model.Categoria;
import it.unisalento.pps1920.carsharing.model.Modello;

import java.util.ArrayList;

public class CategoriaDAO implements ICategoriaDAO {
    @Override
    public Categoria findById(int id) {
        Categoria cat = null;

        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM categoria WHERE id = " + id + ";");

        if(result.size() == 1) {

            cat = new Categoria();
            String riga[] = result.get(0);

            IModelloDAO ModDao = new ModelloDAO();

            Modello modello = ModDao.findById(Integer.parseInt(riga[2]));

            cat.setId(Integer.parseInt(riga[0]));
            cat.setDimensioni(riga[1]);
            cat.setModello(modello);

        }
        return cat;
    }

    @Override
    public ArrayList<Categoria> findAll() {
        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM categoria;");

        ArrayList<Categoria> categoriaArrayList = new ArrayList<Categoria>();

        for (int i = 0; i < result.size(); i++ ) {
            Categoria cat = new Categoria();

            String riga[] = result.get(0);

            IModelloDAO ModDao = new ModelloDAO();

            Modello modello = ModDao.findById(Integer.parseInt(riga[2]));

            cat.setId(Integer.parseInt(riga[0]));
            cat.setDimensioni(riga[1]);
            cat.setModello(modello);


            categoriaArrayList.add(cat);
        }
        return categoriaArrayList;
    }

    @Override
    public void save(Categoria categoria) {

        boolean success = false;
        if (categoria.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE categoria " +
                            "SET " +

                            "dimensioni=" + categoria.getDimensioni() + "," +
                            "modello_id=" + categoria.getModello().getId() + "," +

                            "WHERE id=" + categoria.getId() + ";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO categoria (" +

                            "dimensioni, " +
                            "modello_id)" +

                            "VALUES ('" +
                            categoria.getDimensioni() + "', '" +
                            categoria.getModello().getId() + "');");
        }

    }

    @Override
    public void delete(Categoria categoria) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM foto_auto WHERE id=" + categoria.getId() + ";");
    }
}
