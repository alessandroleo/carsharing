package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.*;
import it.unisalento.pps1920.carsharing.model.*;

import java.util.ArrayList;

public class AccessorioDAO implements IAccessorioDAO {

    @Override
    public Accessorio findById(int id) {

        Accessorio a = null;

        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM accessorio WHERE id = " + id + ";");

        if(result.size() == 1) {

            String riga[] = result.get(0);
            a = new Accessorio();

            IMezzoDAO mDao = new MezzoDAO();

            Mezzo mezzo = mDao.findById(Integer.parseInt(riga[3]));

            a.setId(Integer.parseInt(riga[0]));
            a.setNome(riga[1]);
            a.setPrezzo(Integer.parseInt(riga[2]));
            a.setMezzo(mezzo);

        }

        return a;

    }

    @Override
    public ArrayList<Accessorio> findAll() {
        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM accessorio");

        ArrayList<Accessorio> accessorioArrayList = new ArrayList<Accessorio>();

        for(String[] riga : res) {
            Accessorio a = new Accessorio();

            a.setId(Integer.parseInt(riga[0]));
            a.setNome(riga[1]);
            a.setPrezzo(Integer.parseInt(riga[2]));

            accessorioArrayList.add(a);
        }
        return accessorioArrayList;
    }

    @Override
    public void save(Accessorio accessorio) {
        boolean success = false;
        if (accessorio.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE accessorio " +
                            "SET " +

                            "nome='" + accessorio.getNome() + "'," +
                            "prezzo='" + accessorio.getPrezzo() + "'," +
                            "mezzo='" + accessorio.getMezzo().getId() + "'," +

                            "WHERE id=" + accessorio.getId() + ";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO accessorio (" +
                            "nome, " +
                            "prezzo, " +
                            "mezzo) " +

                            "VALUES ('" +
                            accessorio.getNome() + "', '" +
                            accessorio.getPrezzo() + "', '" +
                            accessorio.getMezzo().getId()+ "');");
        }

    }

    @Override
    public void delete(Accessorio accessorio) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM localita WHERE id="+accessorio.getId()+";");
    }

    @Override
    public ArrayList<Accessorio> findAllByPrenotazioneId(int prenotazione_id) {
        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery(
                "SELECT PA.prenotazione_id, " +
                        "PA.accessorio_id, " +
                        "A.id, " +
                        "A.nome, " +
                        "P.id " +
                        "FROM prenotazione_accessorio as PA INNER JOIN accessorio as A on PA.accessorio_id = A.id " +
                        "INNER JOIN prenotazione as P on PA.prenotazione_id = P.id "+

                        "WHERE prenotazione_id="+prenotazione_id+ ";");

        ArrayList<Accessorio> accessorioArrayList = new ArrayList<Accessorio>();

        for (String[] riga : res) {
            Accessorio a = new Accessorio();

            a.setId(Integer.parseInt(riga[1]));
            a.setNome(riga[3]);

            accessorioArrayList.add(a);
        }
        return accessorioArrayList;
    }
}

