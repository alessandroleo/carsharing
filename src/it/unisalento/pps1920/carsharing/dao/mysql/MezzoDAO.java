package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.*;
import it.unisalento.pps1920.carsharing.model.*;

import java.util.ArrayList;

public class MezzoDAO implements IMezzoDAO {

    @Override
    public Mezzo findById(int id) {

        Mezzo m = null;

        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM mezzo WHERE id = " + id + ";");

        if(result.size() == 1) {

            m = new Mezzo();
            String riga[] = result.get(0);

            IModelloDAO modDao = new ModelloDAO();
            IMarcaDAO marDao = new MarcaDAO();
            IMotorizzazioneDAO motDao = new MotorizzazioneDAO();

            Modello modello = modDao.findById(Integer.parseInt(riga[2]));
            Marca marca = marDao.findById(Integer.parseInt(riga[3]));
            Motorizzazione motorizzazione = motDao.findById(Integer.parseInt(riga[4]));

            m.setId(Integer.parseInt(riga[0]));
            m.setTarga(riga[1]);
            m.setModello(modello);
            m.setMarca(marca);
            m.setMotorizzazione(motorizzazione);
            m.setPrezzo(Integer.parseInt(riga[5]));
            byte[] foto = DbConnection.getInstance().getFoto("SELECT foto FROM mezzo WHERE id = "+id+";");
            m.setFoto(foto);

        }

        return m;
    }

    @Override
    public ArrayList<Mezzo> findAll() {

        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM mezzo;");

        ArrayList<Mezzo> mezzoArrayList = new ArrayList<Mezzo>();

        for (int i = 0; i < result.size(); i++ ) {
            Mezzo m = new Mezzo();

            String riga[] = result.get(i);

            IModelloDAO ModDao = new ModelloDAO();
            IMarcaDAO MarDao = new MarcaDAO();
            IMotorizzazioneDAO MotDao = new MotorizzazioneDAO();

            Modello modello = ModDao.findById(Integer.parseInt(riga[2]));
            Marca marca = MarDao.findById(Integer.parseInt(riga[3]));
            Motorizzazione motorizzazione = MotDao.findById(Integer.parseInt(riga[4]));

            m.setId(Integer.parseInt(riga[0]));
            m.setTarga(riga[1]);
            m.setModello(modello);
            m.setMarca(marca);
            m.setMotorizzazione(motorizzazione);
            m.setPrezzo(Integer.parseInt(riga[5]));
            byte[] foto = DbConnection.getInstance().getFoto("SELECT foto FROM mezzo WHERE id = "+riga[0]+";");
            m.setFoto(foto);

            mezzoArrayList.add(m);
        }

        return mezzoArrayList;
    }

    @Override
    public void save(Mezzo mezzo) {
        boolean success = false;
        if (mezzo.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE mezzo " +
                            "SET " +

                            "targa='" + mezzo.getTarga() + "'," +
                            "modello_id='" + mezzo.getModello().getId() + "'," +
                            "marca_id='" + mezzo.getMarca().getId() + "'," +
                            "motorizzazione_id='" + mezzo.getMotorizzazione().getId() + "' " +
                            "prezzo=" + mezzo.getPrezzo() + "' " +

                            "WHERE id=" + mezzo.getId() + ";");
        } else {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "INSERT INTO mezzo (" +

                            "targa, " +
                            "modello_id, " +
                            "marca_id, " +
                            "motorizzazione_id, " +
                            "prezzo) " +

                            "VALUES ('" +
                            mezzo.getTarga() + "', '" +
                            mezzo.getModello().getId() + "', '" +
                            mezzo.getMarca().getId() + "', '" +
                            mezzo.getMotorizzazione().getId() + "', '" +
                            mezzo.getPrezzo()+ "');");

        }
    }
    @Override
    public void delete (Mezzo mezzo){
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM mezzo WHERE id=" + mezzo.getId() + ";");
    }

    @Override
    public ArrayList<Mezzo> sortByModello() {
        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM mezzo ORDER BY modello_id");

        ArrayList<Mezzo> mezzoArrayList = new ArrayList<Mezzo>();

        for (int i = 0; i < result.size(); i++ ) {
            Mezzo m = new Mezzo();

            String riga[] = result.get(i);

            IModelloDAO ModDao = new ModelloDAO();
            IMarcaDAO MarDao = new MarcaDAO();
            IMotorizzazioneDAO MotDao = new MotorizzazioneDAO();

            Modello modello = ModDao.findById(Integer.parseInt(riga[2]));
            Marca marca = MarDao.findById(Integer.parseInt(riga[3]));
            Motorizzazione motorizzazione = MotDao.findById(Integer.parseInt(riga[4]));

            m.setId(Integer.parseInt(riga[0]));
            m.setTarga(riga[1]);
            m.setModello(modello);
            m.setMarca(marca);
            m.setMotorizzazione(motorizzazione);
            m.setPrezzo(Integer.parseInt(riga[5]));
            byte[] foto = DbConnection.getInstance().getFoto("SELECT foto FROM mezzo WHERE id = " + riga[0] + ";");
            m.setFoto(foto);

            mezzoArrayList.add(m);
        }

        return mezzoArrayList;
    }

    @Override
    public ArrayList<Mezzo> sortByMarca() {
        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM mezzo ORDER BY marca_id");

        ArrayList<Mezzo> mezzoArrayList = new ArrayList<Mezzo>();

        for (int i = 0; i < result.size(); i++ ) {
            Mezzo m = new Mezzo();

            String riga[] = result.get(i);

            IModelloDAO ModDao = new ModelloDAO();
            IMarcaDAO MarDao = new MarcaDAO();
            IMotorizzazioneDAO MotDao = new MotorizzazioneDAO();

            Modello modello = ModDao.findById(Integer.parseInt(riga[2]));
            Marca marca = MarDao.findById(Integer.parseInt(riga[3]));
            Motorizzazione motorizzazione = MotDao.findById(Integer.parseInt(riga[4]));

            m.setId(Integer.parseInt(riga[0]));
            m.setTarga(riga[1]);
            m.setModello(modello);
            m.setMarca(marca);
            m.setMotorizzazione(motorizzazione);
            m.setPrezzo(Integer.parseInt(riga[5]));
            byte[] foto = DbConnection.getInstance().getFoto("SELECT foto FROM mezzo WHERE id = "+riga[0]+";");
            m.setFoto(foto);

            mezzoArrayList.add(m);
        }

        return mezzoArrayList;
    }

    @Override
    public ArrayList<Mezzo> sortByMotorizzazione() {
        ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery("SELECT * FROM mezzo ORDER BY motorizzazione_id");

        ArrayList<Mezzo> mezzoArrayList = new ArrayList<Mezzo>();

        for (int i = 0; i < result.size(); i++ ) {
            Mezzo m = new Mezzo();

            String riga[] = result.get(i);

            IModelloDAO ModDao = new ModelloDAO();
            IMarcaDAO MarDao = new MarcaDAO();
            IMotorizzazioneDAO MotDao = new MotorizzazioneDAO();

            Modello modello = ModDao.findById(Integer.parseInt(riga[2]));
            Marca marca = MarDao.findById(Integer.parseInt(riga[3]));
            Motorizzazione motorizzazione = MotDao.findById(Integer.parseInt(riga[4]));

            m.setId(Integer.parseInt(riga[0]));
            m.setTarga(riga[1]);
            m.setModello(modello);
            m.setMarca(marca);
            m.setMotorizzazione(motorizzazione);
            m.setPrezzo(Integer.parseInt(riga[5]));
            byte[] foto = DbConnection.getInstance().getFoto("SELECT foto FROM mezzo WHERE id = "+riga[0]+";");
            m.setFoto(foto);

            mezzoArrayList.add(m);
        }

        return mezzoArrayList;
    }
}
