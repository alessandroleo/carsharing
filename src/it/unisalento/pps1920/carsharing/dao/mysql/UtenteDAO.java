package it.unisalento.pps1920.carsharing.dao.mysql;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.interfaces.IStazioneDAO;
import it.unisalento.pps1920.carsharing.dao.interfaces.IUtenteDAO;
import it.unisalento.pps1920.carsharing.model.Stazione;
import it.unisalento.pps1920.carsharing.model.Utente;

import java.util.ArrayList;

public class UtenteDAO implements IUtenteDAO {

    @Override
    public Utente findById(int id) {
        Utente u = null;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM utente WHERE id="+id+";");

        if(res.size()==1) {

            String[] riga = res.get(0);
            u = new Utente();

            u.setId(Integer.parseInt(riga[0]));
            u.setNome(riga[1]);
            u.setCognome(riga[2]);
            u.setEmail(riga[3]);
            u.setPassword(riga[4]);
            u.setTelefono(riga[5]);
            u.setResidenza(riga[6]);
            u.setEta(Integer.parseInt(riga[7]));
            byte[] foto = DbConnection.getInstance().getFoto("SELECT foto FROM utente WHERE id = "+id+";");
            u.setFoto(foto);
            u.setRuolo(riga[9]);
        }
        return u;
    }

    @Override
    public Utente findByEmailAndPassword(String email, String password) {
        Utente u = null;

        ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery("SELECT * FROM utente WHERE email='"+email+"' AND password = '"+password+"';");

        if(res.size()==1) {

            String[] riga = res.get(0);

            u = new Utente();

            IStazioneDAO StazioneDAO = new StazioneDAO();

            u.setId(Integer.parseInt(riga[0]));
            u.setNome(riga[1]);
            u.setCognome(riga[2]);
            u.setEmail(riga[3]);
            u.setPassword(riga[4]);
            u.setTelefono(riga[5]);
            u.setResidenza(riga[6]);
            u.setEta(Integer.parseInt(riga[7]));
            byte[] foto = DbConnection.getInstance().getFoto("SELECT foto FROM utente WHERE email='"+email+"' AND password = '"+password+"';");
            u.setFoto(foto);
            u.setRuolo(riga[9]);
            if (u.getRuolo().equals("ROLE_ADDETTO") || u.getRuolo().equals("ROLE_OPERATORE_SPORTELLO")){
                Stazione stazione = StazioneDAO.findById(Integer.parseInt(riga[10]));
                u.setStazione(stazione);
            }
        }
        return u;
    }

    @Override
    public ArrayList<Utente> findAll() {
        ArrayList<String[]> risultato = DbConnection.getInstance().eseguiQuery("SELECT * FROM utente;");

        ArrayList<Utente> utenti = new ArrayList<Utente>();

        for(String[] riga : risultato) {
            Utente u = new Utente();

            u.setId(Integer.parseInt(riga[0]));
            u.setNome(riga[1]);
            u.setCognome(riga[2]);
            u.setEmail(riga[3]);
            u.setPassword(riga[4]);
            u.setTelefono(riga[5]);
            u.setResidenza(riga[6]);
            u.setEta(Integer.parseInt(riga[7]));
            byte[] foto = DbConnection.getInstance().getFoto("SELECT foto FROM utente WHERE id = " + riga[0] + ";");
            u.setFoto(foto);

            utenti.add(u);
        }

        return utenti;
    }

    @Override
    public void save(Utente utente) {
        boolean success = false;
        if (utente.getId() > 0) {
            success = DbConnection.getInstance().eseguiAggiornamento(
                    "UPDATE utente " +
                            "SET " +

                            "nome='"+utente.getNome() + "', " +
                            "cognome='"+utente.getCognome() + "', " +
                            "email='"+utente.getEmail() + "', " +
                            "password='"+utente.getPassword() + "', " +
                            "telefono='"+utente.getTelefono() + "', " +
                            "residenza='"+utente.getResidenza() + "', " +
                            "eta='"+utente.getEta() + "', " +
                            "foto='"+utente.getFoto() + "' " +

                            "WHERE id="+utente.getId()+";");
        } else {
             DbConnection.getInstance().addFoto(utente.getInput(),
                    "INSERT INTO utente (" +
                            "nome," +
                            "cognome, " +
                            "email, " +
                            "password, " +
                            "telefono, " +
                            "residenza, " +
                            "eta, " +
                            "foto, " +
                            "ruolo) " +

                            "VALUES ("+
                            "'" + utente.getNome()+ "', " +
                            "'" + utente.getCognome()+ "', " +
                            "'" + utente.getEmail()+ "', " +
                            "'" + utente.getPassword()+ "', " +
                            "'" + utente.getTelefono()+ "', " +
                            "'" + utente.getResidenza()+ "', " +
                            "'" + utente.getEta() + "', " +
                            "?, " +
                            "'" + utente.getRuolo()+ "');");

        }
    }

    @Override
    public void delete(Utente utente) {
        boolean success = DbConnection.getInstance().eseguiAggiornamento("DELETE FROM cliente WHERE id="+utente.getId()+";");
    }
}
