package it.unisalento.pps1920.carsharing.view.mezzo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MezziHome extends JPanel {
    public JFrame catalogoListForm;;
    public JFrame offerte;

    public MezziHome() {
        super();

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        JButton visualizzaCatalogoBtn = new JButton("Visualizza catalogo");

        JButton visualizzaOfferteBtn = new JButton("Visualizza offerte");


        visualizzaCatalogoBtn.setAlignmentX(visualizzaCatalogoBtn.CENTER_ALIGNMENT);
        visualizzaOfferteBtn.setAlignmentX(visualizzaOfferteBtn.CENTER_ALIGNMENT);


        visualizzaCatalogoBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        visualizzaOfferteBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));


        this.add(visualizzaCatalogoBtn);
        this.add(visualizzaOfferteBtn);

        this.setVisible(true);


        visualizzaOfferteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                offerte = new OfferteFrame();
            }
        });
        visualizzaCatalogoBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                catalogoListForm = new CatalogoFrame();
            }
        });
    }


}





