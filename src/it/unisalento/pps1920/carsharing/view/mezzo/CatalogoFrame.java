package it.unisalento.pps1920.carsharing.view.mezzo;

import it.unisalento.pps1920.carsharing.dao.mysql.MezzoDAO;
import it.unisalento.pps1920.carsharing.model.Mezzo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CatalogoFrame extends JFrame {
    public MezzoDAO mezzoDAO = new MezzoDAO();
    public ArrayList<Mezzo> mezzoList;

    JFrame instance;
    JLabel targaLbl;
    JLabel modelloLbl;
    JLabel marcaLbl;
    JLabel motorizzazioneLbl;
    JLabel numeroPostiLbl;
    JLabel prezzoLbl;

    JButton ordinaPerModelloBtn;
    JButton ordinaPerMarcaBtn;
    JButton ordinaPerMotorizzazioneBtn;

    JPanel buttonPanel = new JPanel();

    public CatalogoFrame() throws HeadlessException {
        super("Catalogo Mezzi");
        this.instance = this;

        //this.setLayout(new FlowLayout());
        this.setSize(600, 700);

        JPanel container = new JPanel();

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setSize(600, 700);

        JScrollPane scroller = new JScrollPane(container);
        scroller.setPreferredSize(new Dimension(580, 660));

        this.setResizable(true);
        this.setVisible(true);
        this.add(scroller);
        scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        mezzoList = mezzoDAO.findAll();

        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.setAlignmentX(LEFT_ALIGNMENT);

        ordinaPerModelloBtn = new JButton("Ordina per modello");
        ordinaPerMarcaBtn = new JButton("Ordina per marca");
        ordinaPerMotorizzazioneBtn = new JButton("Ordina per motorizzazione");

        buttonPanel.add(ordinaPerModelloBtn);
        buttonPanel.add(ordinaPerMarcaBtn);
        buttonPanel.add(ordinaPerMotorizzazioneBtn);

        container.add(buttonPanel);
        container.add(Box.createRigidArea(new Dimension(0, 20)));

        for (int i = 0; i<mezzoList.size(); i++) {

            byte[] img = mezzoList.get(i).getFoto();

            JLabel picLbl = new JLabel(new ImageIcon(img));

            modelloLbl = new JLabel("Modello: " + mezzoList.get(i).getModello().getNome());
            modelloLbl.setFont(new Font("Arial", Font.PLAIN, 20));

            marcaLbl =new JLabel("Marca: " + mezzoList.get(i).getMarca().getNome());
            marcaLbl.setFont(new Font("Arial", Font.PLAIN, 20));

            motorizzazioneLbl =new JLabel("Motorizzazione: "+ mezzoList.get(i).getMotorizzazione().getTipo());
            motorizzazioneLbl.setFont(new Font("Arial", Font.PLAIN, 20));

            numeroPostiLbl = new JLabel("Numero posti: " + mezzoList.get(i).getModello().getNumero_posti());
            numeroPostiLbl.setFont(new Font("Arial", Font.PLAIN, 20));

            targaLbl = new JLabel("Targa: " + mezzoList.get(i).getTarga());
            targaLbl.setFont(new Font("Arial", Font.PLAIN, 20));


            prezzoLbl = new JLabel("Prezzo: " + mezzoList.get(i).getPrezzo());
            prezzoLbl.setFont(new Font("Arial", Font.PLAIN, 20));

            container.add(picLbl);
            container.add(Box.createRigidArea(new Dimension(0, 10)));

            container.add(modelloLbl);
            container.add(marcaLbl);
            container.add(motorizzazioneLbl);
            container.add(numeroPostiLbl);
            container.add(targaLbl);
            container.add(prezzoLbl);

            container.add(Box.createRigidArea(new Dimension(0, 30)));

        }

        ordinaPerModelloBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                container.removeAll();

                buttonPanel.add(ordinaPerModelloBtn);
                buttonPanel.add(ordinaPerMarcaBtn);
                buttonPanel.add(ordinaPerMotorizzazioneBtn);

                container.add(buttonPanel);
                container.add(Box.createRigidArea(new Dimension(0, 20)));

                mezzoList = mezzoDAO.sortByModello();

                for (int i = 0; i<mezzoList.size(); i++) {

                    byte[] img = mezzoList.get(i).getFoto();

                    JLabel picLbl = new JLabel(new ImageIcon(img));

                    modelloLbl = new JLabel("Modello: " + mezzoList.get(i).getModello().getNome());
                    modelloLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    marcaLbl =new JLabel("Marca: " + mezzoList.get(i).getMarca().getNome());
                    marcaLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    motorizzazioneLbl =new JLabel("Tipo: "+ mezzoList.get(i).getMotorizzazione().getTipo());
                    motorizzazioneLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    numeroPostiLbl = new JLabel("Numero posti: " + mezzoList.get(i).getModello().getNumero_posti());
                    numeroPostiLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    targaLbl = new JLabel("Targa: " + mezzoList.get(i).getTarga());
                    targaLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    numeroPostiLbl = new JLabel ("Numero posti: " + mezzoList.get(i).getModello().getNumero_posti());
                    numeroPostiLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    prezzoLbl = new JLabel("Prezzo: " + mezzoList.get(i).getPrezzo());
                    prezzoLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    container.add(picLbl);
                    container.add(modelloLbl);
                    container.add(marcaLbl);
                    container.add(motorizzazioneLbl);
                    container.add(numeroPostiLbl);
                    container.add(targaLbl);
                    container.add(prezzoLbl);

                    container.add(Box.createRigidArea(new Dimension(0, 30)));

                }
            }
        });

        ordinaPerMarcaBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                container.removeAll();

                buttonPanel.add(ordinaPerModelloBtn);
                buttonPanel.add(ordinaPerMarcaBtn);
                buttonPanel.add(ordinaPerMotorizzazioneBtn);

                container.add(buttonPanel);
                container.add(Box.createRigidArea(new Dimension(0, 20)));

                mezzoList = mezzoDAO.sortByMarca();

                for (int i = 0; i<mezzoList.size(); i++) {

                    byte[] img = mezzoList.get(i).getFoto();

                    JLabel picLbl = new JLabel(new ImageIcon(img));

                    modelloLbl = new JLabel("Modello: " + mezzoList.get(i).getModello().getNome());
                    modelloLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    marcaLbl =new JLabel("Marca: " + mezzoList.get(i).getMarca().getNome());
                    marcaLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    motorizzazioneLbl =new JLabel("Tipo: "+ mezzoList.get(i).getMotorizzazione().getTipo());
                    motorizzazioneLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    numeroPostiLbl = new JLabel("Numero posti: " + mezzoList.get(i).getModello().getNumero_posti());
                    numeroPostiLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    targaLbl = new JLabel("Targa: " + mezzoList.get(i).getTarga());
                    targaLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    numeroPostiLbl = new JLabel ("Numero posti: " + mezzoList.get(i).getModello().getNumero_posti());
                    numeroPostiLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    prezzoLbl = new JLabel("Prezzo: " + mezzoList.get(i).getPrezzo());
                    prezzoLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    container.add(picLbl);
                    container.add(modelloLbl);
                    container.add(marcaLbl);
                    container.add(motorizzazioneLbl);
                    container.add(numeroPostiLbl);
                    container.add(targaLbl);
                    container.add(prezzoLbl);

                    container.add(Box.createRigidArea(new Dimension(0, 30)));

                }
            }
        });

        ordinaPerMotorizzazioneBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                container.removeAll();

                buttonPanel.add(ordinaPerModelloBtn);
                buttonPanel.add(ordinaPerMarcaBtn);
                buttonPanel.add(ordinaPerMotorizzazioneBtn);

                container.add(buttonPanel);
                container.add(Box.createRigidArea(new Dimension(0, 20)));

                mezzoList = mezzoDAO.sortByMotorizzazione();

                for (int i = 0; i<mezzoList.size(); i++) {

                    byte[] img = mezzoList.get(i).getFoto();

                    JLabel picLbl = new JLabel(new ImageIcon(img));

                    modelloLbl = new JLabel("Modello: " + mezzoList.get(i).getModello().getNome());
                    modelloLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    marcaLbl =new JLabel("Marca: " + mezzoList.get(i).getMarca().getNome());
                    marcaLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    motorizzazioneLbl =new JLabel("Tipo: "+ mezzoList.get(i).getMotorizzazione().getTipo());
                    motorizzazioneLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    numeroPostiLbl = new JLabel("Numero posti: " + mezzoList.get(i).getModello().getNumero_posti());
                    numeroPostiLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    targaLbl = new JLabel("Targa: " + mezzoList.get(i).getTarga());
                    targaLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    numeroPostiLbl = new JLabel ("Numero posti: " + mezzoList.get(i).getModello().getNumero_posti());
                    numeroPostiLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    prezzoLbl = new JLabel("Prezzo: " + mezzoList.get(i).getPrezzo());
                    prezzoLbl.setFont(new Font("Arial", Font.PLAIN, 20));

                    container.add(picLbl);
                    container.add(modelloLbl);
                    container.add(marcaLbl);
                    container.add(motorizzazioneLbl);
                    container.add(numeroPostiLbl);
                    container.add(targaLbl);
                    container.add(prezzoLbl);

                    container.add(Box.createRigidArea(new Dimension(0, 30)));

                }
            }
        });

        instance.setVisible(true);
    }

}
