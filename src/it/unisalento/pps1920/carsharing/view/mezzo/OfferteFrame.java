package it.unisalento.pps1920.carsharing.view.mezzo;

import javax.swing.*;
import java.awt.*;

public class OfferteFrame extends JFrame {

    JFrame frame;
    JLabel offertaLbl;

    public OfferteFrame() {

        frame = new JFrame();
        frame.setLayout(new FlowLayout());
        offertaLbl = new JLabel("Per un periodo limitato le auto Alfa Romeo elettrica," +
                " Audi diesel e Opel benzina" +
                " saranno disponibili a 180 euro invece che 200," +
                " affrettatevi!");
        offertaLbl.setFont(new Font("Arial", Font.BOLD, 15));

        frame.setSize(1500,300);
        frame.setVisible(true);
        frame.add(offertaLbl);

    }
}
