package it.unisalento.pps1920.carsharing.view.operatore;

import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Session;
import it.unisalento.pps1920.carsharing.view.messaggi.ViewMessaggiFrame;
import it.unisalento.pps1920.carsharing.view.messaggi.WriteMessaggioFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OperatoreUserHome extends JPanel {

    public OperatoreUserHome() {

        super();

        Utente loggedUser =  (Utente) Session.getInstance().get(Session.LOGGED_USER);

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        JButton visualizzaPrenotazioniBtn = new JButton("Visualizza prenotazioni della stazione: " + loggedUser.getStazione().getNome());

        JButton visualizzaMessaggiBtn = new JButton("Visualizza messaggi");

        JButton InviaMessaggieBtn = new JButton("Invia messaggio");

        visualizzaPrenotazioniBtn.setAlignmentX(visualizzaPrenotazioniBtn.CENTER_ALIGNMENT);
        visualizzaMessaggiBtn.setAlignmentX(visualizzaMessaggiBtn.CENTER_ALIGNMENT);
        InviaMessaggieBtn.setAlignmentX(InviaMessaggieBtn.CENTER_ALIGNMENT);

        visualizzaPrenotazioniBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE,Integer.MAX_VALUE));
        visualizzaMessaggiBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        InviaMessaggieBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));

        this.add(visualizzaPrenotazioniBtn);
        this.add(visualizzaMessaggiBtn);
        this.add(InviaMessaggieBtn);

        visualizzaPrenotazioniBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                ViewPrenotazioniOperatore prenotazioneListForm = new ViewPrenotazioniOperatore();
            }
        });
        InviaMessaggieBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                WriteMessaggioFrame messaggioform = new WriteMessaggioFrame();
            }
        });
        visualizzaMessaggiBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                ViewMessaggiFrame visualizzaMessaggiFrame = new ViewMessaggiFrame();
            }
        });

        this.setVisible(true);

    }
}
