package it.unisalento.pps1920.carsharing.view.admin;

import it.unisalento.pps1920.carsharing.dao.mysql.PrenotazioneDAO;
import it.unisalento.pps1920.carsharing.model.Prenotazione;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.PdfHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ViewPrenotazioniAdmin extends JFrame {

    JFrame instance;

    JLabel dataPrenotazioneLbl;
    JLabel dataInizioLbl;
    JLabel dataFineLbl;
    
    JLabel mezzoLbl;
    JLabel localitaLbl;
    JLabel stazionePartenzaLbl;
    JLabel stazioneArrivoLbl;

    JButton ordinaPerDataBtn = new JButton("Ordina per data prenotazione");
    JButton ordinaPerStazioneBtn = new JButton("Ordina per stazione di noleggio");
    JButton ordinaPerMezzoBtn = new JButton("Ordina per mezzo utilizzato");

    JPanel buttonPanel = new JPanel();

    public PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();
    public ArrayList<Prenotazione> prenotazioneList;
    public ArrayList<Utente> clientiList;

    public ViewPrenotazioniAdmin() throws HeadlessException {

        super("Lista prenotazioni");
        this.instance = this;

        this.setSize(700, 550);
        JPanel container = new JPanel();

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setSize(680, 500);

        JScrollPane scroller = new JScrollPane(container);
        scroller.setPreferredSize(new Dimension(680, 500));

        this.setResizable(false);
        this.setVisible(true);
        this.add(scroller);

        scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.setAlignmentX(LEFT_ALIGNMENT);

        buttonPanel.add(ordinaPerDataBtn);
        buttonPanel.add(ordinaPerStazioneBtn);
        buttonPanel.add(ordinaPerMezzoBtn);

        container.add(buttonPanel);
        container.add(Box.createRigidArea(new Dimension(0, 20)));

        prenotazioneList = prenotazioneDAO.findAll();

        ArrayList<String> testo = new ArrayList<>();

        for (int i = 0; i < prenotazioneList.size(); i++) {

            dataPrenotazioneLbl = new JLabel("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione());
            dataPrenotazioneLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            dataInizioLbl = new JLabel("Data di inizio: " + prenotazioneList.get(i).getData_inizio());
            dataInizioLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            dataFineLbl = new JLabel("Data di fine: " + prenotazioneList.get(i).getData_fine());
            dataFineLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            mezzoLbl = new JLabel("Mezzo: " + prenotazioneList.get(i).getMezzo().getModello().getNome() + " " +
                    prenotazioneList.get(i).getMezzo().getMarca().getNome() + " " +
                    prenotazioneList.get(i).getMezzo().getMotorizzazione().getTipo());
            mezzoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            localitaLbl = new JLabel("Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta());
            localitaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            stazionePartenzaLbl = new JLabel("Stazione di partenza: " + prenotazioneList.get(i).getStazione_partenza().getNome());
            stazionePartenzaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            stazioneArrivoLbl = new JLabel("Stazione di arrivo: " + prenotazioneList.get(i).getStazione_arrivo().getNome());
            stazioneArrivoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            testo.add("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione()
                    + " Data di inizio: " + prenotazioneList.get(i).getData_inizio());
            testo.add(" Data di fine: " + prenotazioneList.get(i).getData_fine() +
                    " Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta() +
                    " Stazione di partenza: " + prenotazioneList.get(i).getStazione_partenza().getNome());

            testo.add(" Stazione di arrivo: " + prenotazioneList.get(i).getStazione_arrivo().getNome());
            testo.add("--------------------------------------");

            container.add(dataPrenotazioneLbl);
            container.add(dataInizioLbl);
            container.add(dataFineLbl);

            clientiList = prenotazioneDAO.findAllClientiByPrenotazioneId(prenotazioneList.get(i).getId());
            ArrayList<JLabel> clientiLbl = new ArrayList<JLabel>();

            for (int j = 0; j < clientiList.size(); j++){

                clientiLbl.add(new JLabel("Clienti: " + clientiList.get(j).getNome()));
                clientiLbl.get(j).setFont(new Font("Arial", Font.PLAIN, 15));
                container.add(clientiLbl.get(j));

            }

            container.add(mezzoLbl);
            container.add(localitaLbl);
            container.add(stazionePartenzaLbl);
            container.add(stazioneArrivoLbl);
            container.add(Box.createRigidArea(new Dimension(0, 30)));

        }

        JButton StampaElencoBtn = new JButton("Stampa elenco in pdf");

        StampaElencoBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                PdfHelper.getInstance().creaPdf(testo);
            }
        });

        ordinaPerDataBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                container.removeAll();
                testo.clear();

                buttonPanel.add(ordinaPerDataBtn);
                buttonPanel.add(ordinaPerStazioneBtn);
                buttonPanel.add(ordinaPerMezzoBtn);

                container.add(buttonPanel);
                container.add(Box.createRigidArea(new Dimension(0, 20)));

                prenotazioneList = prenotazioneDAO.sortByData();

                for (int i = 0; i < prenotazioneList.size(); i++) {

                    dataPrenotazioneLbl = new JLabel("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione());
                    dataPrenotazioneLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    dataInizioLbl = new JLabel("Data di inizio: " + prenotazioneList.get(i).getData_inizio());
                    dataInizioLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    dataFineLbl = new JLabel("Data di fine: " + prenotazioneList.get(i).getData_fine());
                    dataFineLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    mezzoLbl = new JLabel("Mezzo: " + prenotazioneList.get(i).getMezzo().getModello().getNome() + " " +
                            prenotazioneList.get(i).getMezzo().getMarca().getNome() + " " +
                            prenotazioneList.get(i).getMezzo().getMotorizzazione().getTipo());
                    mezzoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    localitaLbl = new JLabel("Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta());
                    localitaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    stazionePartenzaLbl = new JLabel("Stazione di partenza: " + prenotazioneList.get(i).getStazione_partenza().getNome());
                    stazionePartenzaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    stazioneArrivoLbl = new JLabel("Stazione di arrivo: " + prenotazioneList.get(i).getStazione_arrivo().getNome());
                    stazioneArrivoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    testo.add("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione()
                            + " Data di inizio: " + prenotazioneList.get(i).getData_inizio());
                    testo.add(" Data di fine: " + prenotazioneList.get(i).getData_fine() +
                            " Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta() +
                            " Stazione di partenza: " + prenotazioneList.get(i).getStazione_partenza().getNome());

                    testo.add(" Stazione di arrivo: " + prenotazioneList.get(i).getStazione_arrivo().getNome());
                    testo.add("--------------------------------------");

                    container.add(dataPrenotazioneLbl);
                    container.add(dataInizioLbl);
                    container.add(dataFineLbl);

                    clientiList = prenotazioneDAO.findAllClientiByPrenotazioneId(prenotazioneList.get(i).getId());
                    ArrayList<JLabel> clientiLbl = new ArrayList<JLabel>();

                    for (int j = 0; j < clientiList.size(); j++){


                        clientiLbl.add(new JLabel("Clienti: " + clientiList.get(j).getNome()));
                        clientiLbl.get(j).setFont(new Font("Arial", Font.PLAIN, 15));
                        container.add(clientiLbl.get(j));

                    }

                    container.add(mezzoLbl);
                    container.add(localitaLbl);
                    container.add(stazionePartenzaLbl);
                    container.add(stazioneArrivoLbl);
                    container.add(Box.createRigidArea(new Dimension(0, 30)));

                }
                container.add(StampaElencoBtn);
            }

        });

        ordinaPerStazioneBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                container.removeAll();
                testo.clear();

                buttonPanel.add(ordinaPerDataBtn);
                buttonPanel.add(ordinaPerStazioneBtn);
                buttonPanel.add(ordinaPerMezzoBtn);

                container.add(buttonPanel);
                container.add(Box.createRigidArea(new Dimension(0, 20)));

                prenotazioneList = prenotazioneDAO.sortByStazione();

                for (int i = 0; i < prenotazioneList.size(); i++) {

                    dataPrenotazioneLbl = new JLabel("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione());
                    dataPrenotazioneLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    dataInizioLbl = new JLabel("Data di inizio: " + prenotazioneList.get(i).getData_inizio());
                    dataInizioLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    dataFineLbl = new JLabel("Data di fine: " + prenotazioneList.get(i).getData_fine());
                    dataFineLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    mezzoLbl = new JLabel("Mezzo: " + prenotazioneList.get(i).getMezzo().getModello().getNome() + " " +
                            prenotazioneList.get(i).getMezzo().getMarca().getNome() + " " +
                            prenotazioneList.get(i).getMezzo().getMotorizzazione().getTipo());
                    mezzoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    localitaLbl = new JLabel("Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta());
                    localitaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    stazionePartenzaLbl = new JLabel("Stazione di partenza: " + prenotazioneList.get(i).getStazione_partenza().getNome());
                    stazionePartenzaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    stazioneArrivoLbl = new JLabel("Stazione di arrivo: " + prenotazioneList.get(i).getStazione_arrivo().getNome());
                    stazioneArrivoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    testo.add("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione()
                            + " Data di inizio: " + prenotazioneList.get(i).getData_inizio());
                    testo.add(" Data di fine: " + prenotazioneList.get(i).getData_fine() +
                            " Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta() +
                            " Stazione di partenza: " + prenotazioneList.get(i).getStazione_partenza().getNome());

                    testo.add(" Stazione di arrivo: " + prenotazioneList.get(i).getStazione_arrivo().getNome());
                    testo.add("--------------------------------------");

                    container.add(dataPrenotazioneLbl);
                    container.add(dataInizioLbl);
                    container.add(dataFineLbl);

                    clientiList = prenotazioneDAO.findAllClientiByPrenotazioneId(prenotazioneList.get(i).getId());
                    ArrayList<JLabel> clientiLbl = new ArrayList<JLabel>();

                    for (int j = 0; j < clientiList.size(); j++){


                        clientiLbl.add(new JLabel("Clienti: " + clientiList.get(j).getNome()));
                        clientiLbl.get(j).setFont(new Font("Arial", Font.PLAIN, 15));
                        container.add(clientiLbl.get(j));

                    }

                    container.add(mezzoLbl);
                    container.add(localitaLbl);
                    container.add(stazionePartenzaLbl);
                    container.add(stazioneArrivoLbl);
                    container.add(Box.createRigidArea(new Dimension(0, 30)));

                }
                container.add(StampaElencoBtn);
            }

        });

        ordinaPerMezzoBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                container.removeAll();
                testo.clear();

                buttonPanel.add(ordinaPerDataBtn);
                buttonPanel.add(ordinaPerStazioneBtn);
                buttonPanel.add(ordinaPerMezzoBtn);

                container.add(buttonPanel);
                container.add(Box.createRigidArea(new Dimension(0, 20)));

                prenotazioneList = prenotazioneDAO.sortByMezzo();

                for (int i = 0; i < prenotazioneList.size(); i++) {

                    dataPrenotazioneLbl = new JLabel("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione());
                    dataPrenotazioneLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    dataInizioLbl = new JLabel("Data di inizio: " + prenotazioneList.get(i).getData_inizio());
                    dataInizioLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    dataFineLbl = new JLabel("Data di fine: " + prenotazioneList.get(i).getData_fine());
                    dataFineLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    mezzoLbl = new JLabel("Mezzo: " + prenotazioneList.get(i).getMezzo().getModello().getNome() + " " +
                            prenotazioneList.get(i).getMezzo().getMarca().getNome() + " " +
                            prenotazioneList.get(i).getMezzo().getMotorizzazione().getTipo());
                    mezzoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    localitaLbl = new JLabel("Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta());
                    localitaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    stazionePartenzaLbl = new JLabel("Stazione di partenza: " + prenotazioneList.get(i).getStazione_partenza().getNome());
                    stazionePartenzaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    stazioneArrivoLbl = new JLabel("Stazione di arrivo: " + prenotazioneList.get(i).getStazione_arrivo().getNome());
                    stazioneArrivoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    testo.add("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione()
                            + " Data di inizio: " + prenotazioneList.get(i).getData_inizio());
                    testo.add(" Data di fine: " + prenotazioneList.get(i).getData_fine() +
                            " Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta() +
                            " Stazione di partenza: " + prenotazioneList.get(i).getStazione_partenza().getNome());

                    testo.add(" Stazione di arrivo: " + prenotazioneList.get(i).getStazione_arrivo().getNome());
                    testo.add("--------------------------------------");

                    container.add(dataPrenotazioneLbl);
                    container.add(dataInizioLbl);
                    container.add(dataFineLbl);

                    clientiList = prenotazioneDAO.findAllClientiByPrenotazioneId(prenotazioneList.get(i).getId());
                    ArrayList<JLabel> clientiLbl = new ArrayList<JLabel>();

                    for (int j = 0; j < clientiList.size(); j++){


                        clientiLbl.add(new JLabel("Clienti: " + clientiList.get(j).getNome()));
                        clientiLbl.get(j).setFont(new Font("Arial", Font.PLAIN, 15));
                        container.add(clientiLbl.get(j));

                    }

                    container.add(mezzoLbl);
                    container.add(localitaLbl);
                    container.add(stazionePartenzaLbl);
                    container.add(stazioneArrivoLbl);
                    container.add(Box.createRigidArea(new Dimension(0, 30)));

                }
                container.add(StampaElencoBtn);
            }

        });
    }
}