package it.unisalento.pps1920.carsharing.view.admin;

import it.unisalento.pps1920.carsharing.view.messaggi.ViewMessaggiFrame;
import it.unisalento.pps1920.carsharing.view.messaggi.WriteMessaggioFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminFrame extends JPanel {

    public AdminFrame () throws HeadlessException {

        super();

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        JButton visualizzaPrenotazioniBtn = new JButton("Visualizza prenotazioni");

        JButton visualizzaMessaggiBtn = new JButton("Visualizza messaggi");

        JButton InviaMessaggieBtn = new JButton("Invia messaggi");

        visualizzaPrenotazioniBtn.setAlignmentX(visualizzaPrenotazioniBtn.CENTER_ALIGNMENT);
        visualizzaMessaggiBtn.setAlignmentX(visualizzaMessaggiBtn.CENTER_ALIGNMENT);
        InviaMessaggieBtn.setAlignmentX(InviaMessaggieBtn.CENTER_ALIGNMENT);

        visualizzaPrenotazioniBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE,Integer.MAX_VALUE));
        visualizzaMessaggiBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        InviaMessaggieBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));

        this.add(visualizzaPrenotazioniBtn);
        this.add(visualizzaMessaggiBtn);
        this.add(InviaMessaggieBtn);

        visualizzaPrenotazioniBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                ViewPrenotazioniAdmin prenotazioneListForm = new ViewPrenotazioniAdmin();
            }
        });
        InviaMessaggieBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                WriteMessaggioFrame messaggioFrame = new WriteMessaggioFrame();
            }
        });
        visualizzaMessaggiBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                ViewMessaggiFrame visualizzaMessaggiFrame = new ViewMessaggiFrame();
            }
        });


        this.setVisible(true);


    }
}
