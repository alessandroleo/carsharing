package it.unisalento.pps1920.carsharing.view.prenotazione;


import it.unisalento.pps1920.carsharing.view.mezzo.CatalogoFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MenuPrenotazioniPanel extends JPanel {

    public JFrame newPrenotazioneForm;;
    public JFrame prenotazioneListForm;
    public JFrame catalogoListForm;

    public MenuPrenotazioniPanel() {
        super();


        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));


            JButton visualizzaPrenotazioniBtn = new JButton("Visualizza prenotazioni");

            JButton visualizzaCatalogoBtn = new JButton("Visualizza catalogo");

            JButton newPrenotazioneBtn = new JButton("Effettua una nuova prenotazione");

            visualizzaPrenotazioniBtn.setAlignmentX(visualizzaPrenotazioniBtn.CENTER_ALIGNMENT);
            visualizzaCatalogoBtn.setAlignmentX(visualizzaCatalogoBtn.CENTER_ALIGNMENT);
            newPrenotazioneBtn.setAlignmentX(newPrenotazioneBtn.CENTER_ALIGNMENT);

            visualizzaPrenotazioniBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE,Integer.MAX_VALUE));
            visualizzaCatalogoBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
            newPrenotazioneBtn.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));

            this.add(visualizzaPrenotazioniBtn);
            this.add(visualizzaCatalogoBtn);
            this.add(newPrenotazioneBtn);

            this.setVisible(true);


            newPrenotazioneBtn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    newPrenotazioneForm = new NewPrenotazioneFrame();

                }
            });

            visualizzaPrenotazioniBtn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    prenotazioneListForm = new ViewPrenotazioniFrame();

                }
            });

            visualizzaCatalogoBtn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    catalogoListForm = new CatalogoFrame();

                }
            });
        }

    }




