package it.unisalento.pps1920.carsharing.view.prenotazione;

import it.unisalento.pps1920.carsharing.dao.mysql.*;
import it.unisalento.pps1920.carsharing.model.*;
import it.unisalento.pps1920.carsharing.util.Session;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EditPrenotazioneFrame extends JFrame{

    EditPrenotazioneFrame instance;
    public JFrame accessoriForm;
    private String dates[]
            = { "01", "02", "03", "04", "05",
            "06", "07", "08", "09", "10",
            "11", "12", "13", "14", "15",
            "16", "17", "18", "19", "20",
            "21", "22", "23", "24", "25",
            "26", "27", "28", "29", "30",
            "31" };
    private String months[]
            = { "Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "July", "Aug",
            "Sup", "Oct", "Nov", "Dec" };
    private String years[]
            = {"2020", "2021", "2022", "2023", "2024"};

    public LocalitaDAO localitaDAO = new LocalitaDAO();
    public ArrayList<Localita> localitaList;

    public StazioneDAO stazioneDAO = new StazioneDAO();
    public ArrayList<Stazione> stazioneList;

    public MezzoDAO mezzoDAO = new MezzoDAO();
    public ArrayList<Mezzo> mezzoList;

    public AccessorioDAO accessorioDAO = new AccessorioDAO();
    public ArrayList<Accessorio> accessoriList;

    JButton saveButton = new JButton("Salva");
    JButton annullaButton = new JButton("Annulla");

    JLabel DataInizioLbl = new JLabel("Data inizio: ");
    JComboBox dateStartCB = new JComboBox(dates);
    JComboBox monthStartCB = new JComboBox(months);
    JComboBox yearStartCB = new JComboBox(years);

    JLabel DataFineLbl = new JLabel("Data fine:");
    JComboBox dateEndCB = new JComboBox(dates);
    JComboBox monthEndCB = new JComboBox(months);
    JComboBox yearEndCB = new JComboBox(years);

    JLabel mezzoLbl = new JLabel("Mezzo: ");
    JComboBox mezzoCB;

    JLabel accessoriLbl = new JLabel("Accessori: ");
    ArrayList<JCheckBox> accessorio = new ArrayList<JCheckBox>();
    ArrayList<JLabel> prezzo = new ArrayList<JLabel>();

    JLabel localitaLbl = new JLabel("Localita: ");
    JComboBox localitaCB;

    JLabel stazionePartenzaLbl = new JLabel("Stazione di partenza:  ");
    JComboBox stazionePartenzaCB;

    JLabel stazioneArrivoLbl = new JLabel("Stazione di arrivo: ");
    JComboBox stazioneArrivoCB;

    public EditPrenotazioneFrame(Prenotazione prenotazione) throws HeadlessException {
        super("Modifica prenotazione");
        this.instance = this;

        Container p = this.getContentPane();

        localitaList = localitaDAO.findAll();
        stazioneList = stazioneDAO.findAll();
        mezzoList = mezzoDAO.findAll();
        accessoriList = accessorioDAO.findAll();

        p.setLayout(null);

        DataInizioLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        DataInizioLbl.setSize(120, 20);
        DataInizioLbl.setLocation(50, 50);

        dateStartCB.setFont(new Font("Arial", Font.PLAIN, 15));
        dateStartCB.setSize(50, 20);
        dateStartCB.setLocation(250, 50);

        monthStartCB.setFont(new Font("Arial", Font.PLAIN, 15));
        monthStartCB.setSize(100, 20);
        monthStartCB.setLocation(300, 50);

        yearStartCB.setFont(new Font("Arial", Font.PLAIN, 15));
        yearStartCB.setSize(150, 20);
        yearStartCB.setLocation(400, 50);

        p.add(DataInizioLbl);
        p.add(dateStartCB);
        p.add(monthStartCB);
        p.add(yearStartCB);



        DataFineLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        DataFineLbl.setSize(120, 20);
        DataFineLbl.setLocation(50, 100);

        dateEndCB.setFont(new Font("Arial", Font.PLAIN, 15));
        dateEndCB.setSize(50, 20);
        dateEndCB.setLocation(250, 100);

        monthEndCB.setFont(new Font("Arial", Font.PLAIN, 15));
        monthEndCB.setSize(100, 20);
        monthEndCB.setLocation(300, 100);

        yearEndCB.setFont(new Font("Arial", Font.PLAIN, 15));
        yearEndCB.setSize(150, 20);
        yearEndCB.setLocation(400, 100);

        p.add(DataFineLbl);
        p.add(dateEndCB);
        p.add(monthEndCB);
        p.add(yearEndCB);



        mezzoLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        mezzoLbl.setSize(120, 20);
        mezzoLbl.setLocation(50, 150);

        mezzoCB = new JComboBox(new DefaultComboBoxModel(mezzoList.toArray()));
        mezzoCB.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if(value instanceof Mezzo){
                    Mezzo mezzo = (Mezzo) value;
                    setText(mezzo.getModello().getNome() + " " + mezzo.getMarca().getNome() + " " + mezzo.getMotorizzazione().getTipo());

                }
                return this;
            }
        } );
        mezzoCB.setFont(new Font("Arial", Font.PLAIN, 15));
        mezzoCB.setSize(300, 20);
        mezzoCB.setLocation(250, 150);

        p.add(mezzoLbl);
        p.add(mezzoCB);



        accessoriLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        accessoriLbl.setSize(120, 20);
        accessoriLbl.setLocation(50, 200);

        p.add(accessoriLbl);

        for (int i = 0; i < accessoriList.size(); i++) {

            accessorio.add(new JCheckBox(accessoriList.get(i).getNome()));

            boolean accessorioIsPresent = false;
            for (int j = 0; j < prenotazione.getAccessori().size(); j++) {
                if (prenotazione.getAccessori().get(j).getId() == accessoriList.get(i).getId()) {
                    accessorioIsPresent = true;
                }
            }

            if (accessorioIsPresent) {
                accessorio.get(i).setSelected(true);
            } else {
                accessorio.get(i).setSelected(false);
            }

            prezzo.add(new JLabel((accessoriList.get(i).getPrezzo() + " €")));

            accessorio.get(i).setSize(200, 30);
            accessorio.get(i).setLocation(250, (200+(i*30)));
            prezzo.get(i).setLocation(450, (200+(i*30)));
            prezzo.get(i).setSize(200, 30);
            p.add(accessorio.get(i));
            p.add(prezzo.get(i));
        }


        localitaLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        localitaLbl.setSize(120, 20);
        localitaLbl.setLocation(50, 380);

        localitaCB = new JComboBox(localitaList.toArray());
        localitaCB.setFont(new Font("Arial", Font.PLAIN, 15));
        localitaCB.setSize(300, 20);
        localitaCB.setLocation(250, 380);

        p.add(localitaLbl);
        p.add(localitaCB);



        stazionePartenzaLbl.setFont(new Font("Arial", Font.PLAIN, 15));
        stazionePartenzaLbl.setSize(200, 20);
        stazionePartenzaLbl.setLocation(50, 430);

        stazionePartenzaCB = new JComboBox(stazioneList.toArray());
        stazionePartenzaCB.setFont(new Font("Arial", Font.PLAIN, 15));
        stazionePartenzaCB.setSize(300, 20);
        stazionePartenzaCB.setLocation(250, 430);

        p.add(stazionePartenzaLbl);
        p.add(stazionePartenzaCB);



        stazioneArrivoLbl.setFont(new Font("Arial", Font.PLAIN, 15));
        stazioneArrivoLbl.setSize(200, 20);
        stazioneArrivoLbl.setLocation(50, 480);

        stazioneArrivoCB = new JComboBox(stazioneList.toArray());
        stazioneArrivoCB.setFont(new Font("Arial", Font.PLAIN, 15));
        stazioneArrivoCB.setSize(300, 20);
        stazioneArrivoCB.setLocation(250, 480);

        p.add(stazioneArrivoLbl);
        p.add(stazioneArrivoCB);



        Dimension saveSize = saveButton.getPreferredSize();
        saveButton.setBounds(250, 550, saveSize.width, saveSize.height);
        Dimension annullaSize = annullaButton.getPreferredSize();
        annullaButton.setBounds(325, 550, annullaSize.width, annullaSize.height);

        p.add(annullaButton);
        p.add(saveButton);



        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String monthStartConvertedCB = null;
                String monthEndConvertedCB = null;

                if (e.getSource() == saveButton) {
                    String dateStart = dateStartCB.getSelectedItem().toString();
                    String monthStart = monthStartCB.getSelectedItem().toString();
                    switch(monthStart) {
                        case "Jan":
                            monthStartConvertedCB = "01";
                            break;

                        case "Feb":
                            monthStartConvertedCB = "02";
                            break;

                        case "Mar":
                            monthStartConvertedCB = "03";
                            break;

                        case "Apr":
                            monthStartConvertedCB = "04";
                            break;

                        case "May":
                            monthStartConvertedCB = "05";
                            break;

                        case "Jun":
                            monthStartConvertedCB = "06";
                            break;

                        case "July":
                            monthStartConvertedCB = "07";
                            break;

                        case "Aug":
                            monthStartConvertedCB = "08";
                            break;

                        case "Sep":
                            monthStartConvertedCB = "09";
                            break;

                        case "Oct":
                            monthStartConvertedCB = "10";
                            break;

                        case "Nov":
                            monthStartConvertedCB = "11";
                            break;

                        case "Dec":
                            monthStartConvertedCB = "12";
                            break;
                    }

                    String yearStart = yearStartCB.getSelectedItem().toString();
                    String dateStartTime = yearStart + "-" + monthStartConvertedCB + "-" + dateStart+ " 00:00:00";


                    String dateEnd = dateEndCB.getSelectedItem().toString();
                    String monthEnd = monthEndCB.getSelectedItem().toString();
                    switch(monthEnd) {
                        case "Jan":
                            monthEndConvertedCB = "01";
                            break;

                        case "Feb":
                            monthEndConvertedCB = "02";
                            break;

                        case "Mar":
                            monthEndConvertedCB = "03";
                            break;

                        case "Apr":
                            monthEndConvertedCB = "04";
                            break;

                        case "May":
                            monthEndConvertedCB = "05";
                            break;

                        case "Jun":
                            monthEndConvertedCB = "06";
                            break;

                        case "July":
                            monthEndConvertedCB = "07";
                            break;

                        case "Aug":
                            monthEndConvertedCB = "08";
                            break;

                        case "Sep":
                            monthEndConvertedCB = "09";
                            break;

                        case "Oct":
                            monthEndConvertedCB = "10";
                            break;

                        case "Nov":
                            monthEndConvertedCB = "11";
                            break;

                        case "Dec":
                            monthEndConvertedCB = "12";
                            break;
                    }
                    String yearEnd = yearEndCB.getSelectedItem().toString();
                    String dateEndTime = yearEnd + "-" + monthEndConvertedCB + "-" + dateEnd+ " 00:00:00";

                    SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date4= new Date(System.currentTimeMillis());

                    Utente loggedUser =  (Utente)Session.getInstance().get(Session.LOGGED_USER);

                    Mezzo mezzo = (Mezzo) mezzoCB.getSelectedItem();

                    Localita localita = (Localita) localitaCB.getSelectedItem();

                    Stazione stazionePartenza = (Stazione) stazionePartenzaCB.getSelectedItem();

                    Stazione stazioneArrivo = (Stazione) stazioneArrivoCB.getSelectedItem();

                    PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();

                    int prezzoAccessori = 0;

                    ArrayList<Accessorio> accessoriToSave = new ArrayList<>();
                    for (int i = 0; i < accessoriList.size(); i++) {
                        if (accessorio.get(i).isSelected()) {
                            accessoriToSave.add(accessoriList.get(i));
                        }
                    }

                    for (int i = 0; i < accessoriToSave.size(); i++){
                        prezzoAccessori += accessoriToSave.get(i).getPrezzo();
                    }

                    int prezzoTotale = mezzo.getPrezzo() + prezzoAccessori;

                    prenotazione.setData_prenotazione(formatter.format(date4));
                    prenotazione.setData_inizio(dateStartTime);
                    prenotazione.setData_fine(dateEndTime);
                    prenotazione.setPrezzoFinale(prezzoTotale);
                    prenotazione.setCliente(loggedUser);
                    prenotazione.setMezzo(mezzo);
                    prenotazione.setLocalita(localita);
                    prenotazione.setStazione_partenza(stazionePartenza);
                    prenotazione.setStazione_arrivo(stazioneArrivo);
                    prenotazione.setPagato(1);
                    prenotazione.setSharable(1);
                    prenotazione.setAccessori(accessoriToSave);

                    int res = JOptionPane.showConfirmDialog(null," Il prezzo totale è: " + prezzoTotale + ", salvare prenotazione?");
                    if (res==JOptionPane.YES_OPTION){
                        prenotazioneDAO.save(prenotazione);
                        JOptionPane.showMessageDialog(null, "Prenotazione salvata!");
                        instance.dispose();
                    }
                }
            }
        });

        annullaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                instance.dispose();

            }
        });

        setBounds(300, 90, 1000, 800);
        setResizable(false);
        setVisible(true);
    }

}

