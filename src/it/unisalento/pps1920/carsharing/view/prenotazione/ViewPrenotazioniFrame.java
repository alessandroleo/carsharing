package it.unisalento.pps1920.carsharing.view.prenotazione;

import it.unisalento.pps1920.carsharing.dao.mysql.PrenotazioneDAO;
import it.unisalento.pps1920.carsharing.model.Prenotazione;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Session;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class ViewPrenotazioniFrame extends JFrame {

    public JFrame instance;

    JLabel numeroPrenotazioneLbl;
    JLabel dataPrenotazioneLbl;
    JLabel dataInizioLbl;
    JLabel dataFineLbl;
    JLabel mezzoLbl;
    JLabel localitaLbl;
    JLabel stazionePartenzaLbl;
    JLabel stazioneArrivoLbl;
    JLabel prezzoLbl;
    JLabel nessunaPrenotazioneLbl;

    public JFrame editPrenotazioneForm;
    public PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();
    public ArrayList<Prenotazione> prenotazioneList;

    public ViewPrenotazioniFrame() throws HeadlessException {

        super("Lista prenotazioni");
        this.instance = this;
        this.setLayout(new FlowLayout());
        this.setSize(520, 550);
        JPanel container = new JPanel();

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setSize(450, 500);

        JScrollPane scroller = new JScrollPane(container);
        scroller.setPreferredSize(new Dimension(500, 500));

        this.setResizable(false);
        this.setVisible(true);
        this.add(scroller);
        scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);


        Utente loggedUser = (Utente) Session.getInstance().get(Session.LOGGED_USER);

        prenotazioneList = prenotazioneDAO.findAllByClienteId(loggedUser.getId());

        if (prenotazioneList.size() > 0) {
            for (int i = 0; i < prenotazioneList.size(); i++) {

                numeroPrenotazioneLbl = new JLabel("Prenotazione N°: " + (i + 1));
                numeroPrenotazioneLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                dataPrenotazioneLbl = new JLabel("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione());
                dataPrenotazioneLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                dataInizioLbl = new JLabel("Data di inizio noleggio: " + prenotazioneList.get(i).getData_inizio());
                dataInizioLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                dataFineLbl = new JLabel("Data di fine noleggio: " + prenotazioneList.get(i).getData_fine());
                dataFineLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                mezzoLbl = new JLabel(
                        "Mezzo: " +
                                prenotazioneList.get(i).getMezzo().getModello().getNome() + " " +
                                prenotazioneList.get(i).getMezzo().getMarca().getNome() + " " +
                                prenotazioneList.get(i).getMezzo().getMotorizzazione().getTipo());
                mezzoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                localitaLbl = new JLabel("Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta());
                localitaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                stazionePartenzaLbl = new JLabel("Stazione di partenza: " + prenotazioneList.get(i).getStazione_partenza().getNome());
                stazionePartenzaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                stazioneArrivoLbl = new JLabel("Stazione di arrivo: " + prenotazioneList.get(i).getStazione_arrivo().getNome());
                stazioneArrivoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                prezzoLbl = new JLabel("Prezzo prenotazione: " + prenotazioneList.get(i).getPrezzoFinale());
                prezzoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                JPanel buttonsPanel = new JPanel();
                buttonsPanel.setLayout(new FlowLayout());
                buttonsPanel.setAlignmentX(FlowLayout.LEFT);

                JButton modificaBtn = new JButton("Modifica");
                JButton eliminaBtn = new JButton("Elimina");

                buttonsPanel.add(modificaBtn);
                buttonsPanel.add(eliminaBtn);

                container.add(numeroPrenotazioneLbl);
                container.add(dataPrenotazioneLbl);
                container.add(dataInizioLbl);
                container.add(dataFineLbl);
                container.add(mezzoLbl);
                container.add(localitaLbl);
                container.add(stazionePartenzaLbl);
                container.add(stazioneArrivoLbl);
                container.add(prezzoLbl);
                container.add(buttonsPanel);

                container.add(Box.createRigidArea(new Dimension(0, 30)));


                int j = i;
                modificaBtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        editPrenotazioneForm = new EditPrenotazioneFrame(prenotazioneList.get(j));
                        SwingUtilities.updateComponentTreeUI(instance);
                    }
                });

                eliminaBtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int res = JOptionPane.showConfirmDialog(
                                null, "Hai scelto di eliminare la prenotazione N° " + (j + 1) + ", confermi?");
                        if (res == JOptionPane.YES_OPTION) {
                            prenotazioneDAO.deleteByClienteId(loggedUser.getId(), prenotazioneList.get(j).getId());
                            JOptionPane.showMessageDialog(null, "Prenotazione eliminata");
                            instance.invalidate();
                            instance.validate();
                            instance.repaint();
                        }
                    }
                });
            }
        } else {
            nessunaPrenotazioneLbl = new JLabel("Non hai ancora effettuato nessuna prenotazione.");
            container.add(nessunaPrenotazioneLbl);
        }
    }
}


