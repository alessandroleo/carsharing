package it.unisalento.pps1920.carsharing.view.prenotazione;

import it.unisalento.pps1920.carsharing.dao.mysql.PrenotazioneDAO;
import it.unisalento.pps1920.carsharing.model.Prenotazione;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Session;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class RichiestaSharingFrame extends JFrame {

    RichiestaSharingFrame instance;
    public ArrayList<Utente> clientiList;

    JLabel messageLbl = new JLabel("Vuoi condividere la prenotazione con questi utenti?");

    JLabel nomeLbl;
    JLabel cognomeLbl;
    JLabel etaLbl;
    JLabel emailLbl;
    JLabel telefonoLbl;
    JLabel residenzaLbl;

    JPanel buttonPanel = new JPanel();

    JButton acceptBtn = new JButton("Accetta");
    JButton refuseBtn = new JButton("Rifiuta");

    private PrenotazioneDAO pDao = new PrenotazioneDAO();

    public RichiestaSharingFrame(int prenotazioneId, Prenotazione p) throws HeadlessException {
        super("Richiesta SHARING");

        this.instance = this;

        this.setSize(620, 550);

        JPanel container = new JPanel();

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setSize(600, 500);

        JScrollPane scroller = new JScrollPane(container);
        scroller.setPreferredSize(new Dimension(600, 500));

        this.setResizable(false);
        this.setVisible(true);
        this.add(scroller);
        scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        clientiList = pDao.findAllClientiByPrenotazioneId(prenotazioneId);

        messageLbl.setFont(new Font("Arial", Font.BOLD, 15));
        container.add(messageLbl);
        container.add(Box.createRigidArea(new Dimension(0, 30)));


        for (int i = 0; i < clientiList.size(); i++) {

            nomeLbl = new JLabel("Nome: " + clientiList.get(i).getNome());
            nomeLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            cognomeLbl = new JLabel("Cognome: " + clientiList.get(i).getCognome());
            cognomeLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            etaLbl = new JLabel("Eta: " + clientiList.get(i).getEta());
            etaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            emailLbl = new JLabel("Email: " + clientiList.get(i).getEmail());
            emailLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            telefonoLbl = new JLabel("Telefono: " + clientiList.get(i).getTelefono());
            telefonoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            residenzaLbl = new JLabel("Residenza: " + clientiList.get(i).getResidenza());
            residenzaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            byte[] img = clientiList.get(i).getFoto();
            JLabel foto = new JLabel(new ImageIcon(img));

            container.add(foto);
            container.add(Box.createRigidArea(new Dimension(0, 10)));

            container.add(nomeLbl);
            container.add(cognomeLbl);
            container.add(etaLbl);
            container.add(emailLbl);
            container.add(telefonoLbl);
            container.add(residenzaLbl);
            container.add(Box.createRigidArea(new Dimension(0, 20)));

        }

        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.setAlignmentX(LEFT_ALIGNMENT);

        buttonPanel.add(acceptBtn);
        buttonPanel.add(refuseBtn);

        container.add(buttonPanel);

        Utente loggedUser =  (Utente) Session.getInstance().get(Session.LOGGED_USER);

        PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();

        acceptBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                prenotazioneDAO.addNewCliente(loggedUser.getId(), prenotazioneId);
                instance.dispose();
            }
        });


        refuseBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                p.setSharable(0);
                prenotazioneDAO.save(p);
                instance.dispose();

            }
        });
    }
}
