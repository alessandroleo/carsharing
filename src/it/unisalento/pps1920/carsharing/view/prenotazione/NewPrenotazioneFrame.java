package it.unisalento.pps1920.carsharing.view.prenotazione;

import it.unisalento.pps1920.carsharing.DbConnection;
import it.unisalento.pps1920.carsharing.dao.mysql.*;
import it.unisalento.pps1920.carsharing.model.*;
import it.unisalento.pps1920.carsharing.util.Session;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NewPrenotazioneFrame extends JFrame {

    NewPrenotazioneFrame instance;

    private String dates[]
            = { "01", "02", "03", "04", "05",
            "06", "07", "08", "09", "10",
            "11", "12", "13", "14", "15",
            "16", "17", "18", "19", "20",
            "21", "22", "23", "24", "25",
            "26", "27", "28", "29", "30",
            "31" };
    private String months[]
            = { "Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "July", "Aug",
            "Sup", "Oct", "Nov", "Dec" };
    private String years[]
            = {"2020", "2021", "2022", "2023", "2024"};

    public LocalitaDAO localitaDAO = new LocalitaDAO();
    public ArrayList<Localita> localitaList;

    public StazioneDAO stazioneDAO = new StazioneDAO();
    public ArrayList<Stazione> stazioneList;

    public MezzoDAO mezzoDAO = new MezzoDAO();
    public ArrayList<Mezzo> mezzoList;

    public AccessorioDAO accessorioDAO = new AccessorioDAO();
    public ArrayList<Accessorio> accessoriList;

    JButton saveButton = new JButton("Salva");
    JButton annullaButton = new JButton("Annulla");

    JLabel DataInizioLbl = new JLabel("Data inizio: ");
    JComboBox dateStartBox = new JComboBox(dates);
    JComboBox monthStartBox = new JComboBox(months);
    JComboBox yearStartBox = new JComboBox(years);

    JLabel DataFineLbl = new JLabel("Data fine:");
    JComboBox dateEndBox = new JComboBox(dates);
    JComboBox monthEndBox = new JComboBox(months);
    JComboBox yearEndBox = new JComboBox(years);

    JLabel mezzoLbl = new JLabel("Mezzo: ");
    JComboBox mezzoBox;

    JLabel accessoriLbl = new JLabel("Accessori: ");
    ArrayList<JCheckBox> accessorio = new ArrayList<JCheckBox>();
    ArrayList<JLabel> prezzo = new ArrayList<JLabel>();

    JLabel localitaLbl = new JLabel("Localita: ");
    JComboBox localitaBox;

    JLabel stazionePartenzaLbl = new JLabel("Stazione di partenza:  ");
    JComboBox stazionePartenzaBox;

    JLabel stazioneArrivoLbl = new JLabel("Stazione di arrivo: ");
    JComboBox stazioneArrivoBox;

    public RichiestaSharingFrame richiestaSharingFrame;



    public NewPrenotazioneFrame() throws HeadlessException {
        super("Prenotazione");
        instance = this;

        Container p = this.getContentPane();

        localitaList = localitaDAO.findAll();
        stazioneList = stazioneDAO.findAll();
        mezzoList = mezzoDAO.findAll();
        accessoriList = accessorioDAO.findAll();


        p.setLayout(null);

        DataInizioLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        DataInizioLbl.setSize(120, 20);
        DataInizioLbl.setLocation(50, 50);

        dateStartBox.setFont(new Font("Arial", Font.PLAIN, 15));
        dateStartBox.setSize(50, 20);
        dateStartBox.setLocation(250, 50);

        monthStartBox.setFont(new Font("Arial", Font.PLAIN, 15));
        monthStartBox.setSize(100, 20);
        monthStartBox.setLocation(300, 50);

        yearStartBox.setFont(new Font("Arial", Font.PLAIN, 15));
        yearStartBox.setSize(150, 20);
        yearStartBox.setLocation(400, 50);

        p.add(DataInizioLbl);
        p.add(dateStartBox);
        p.add(monthStartBox);
        p.add(yearStartBox);



        DataFineLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        DataFineLbl.setSize(120, 20);
        DataFineLbl.setLocation(50, 100);

        dateEndBox.setFont(new Font("Arial", Font.PLAIN, 15));
        dateEndBox.setSize(50, 20);
        dateEndBox.setLocation(250, 100);

        monthEndBox.setFont(new Font("Arial", Font.PLAIN, 15));
        monthEndBox.setSize(100, 20);
        monthEndBox.setLocation(300, 100);

        yearEndBox.setFont(new Font("Arial", Font.PLAIN, 15));
        yearEndBox.setSize(150, 20);
        yearEndBox.setLocation(400, 100);

        p.add(DataFineLbl);
        p.add(dateEndBox);
        p.add(monthEndBox);
        p.add(yearEndBox);



        mezzoLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        mezzoLbl.setSize(120, 20);
        mezzoLbl.setLocation(50, 150);

        mezzoBox = new JComboBox(new DefaultComboBoxModel(mezzoList.toArray()));
        mezzoBox.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if(value instanceof Mezzo){
                    Mezzo mezzo = (Mezzo) value;
                    setText(mezzo.getModello().getNome() + " " + mezzo.getMarca().getNome() + " " + mezzo.getMotorizzazione().getTipo());

                }
                return this;
            }
        } );
        mezzoBox.setFont(new Font("Arial", Font.PLAIN, 15));
        mezzoBox.setSize(300, 20);
        mezzoBox.setLocation(250, 150);

        p.add(mezzoLbl);
        p.add(mezzoBox);



        accessoriLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        accessoriLbl.setSize(120, 20);
        accessoriLbl.setLocation(50, 200);

        p.add(accessoriLbl);

        for (int i = 0; i < accessoriList.size(); i++) {
            accessorio.add(new JCheckBox(accessoriList.get(i).getNome()));
            accessorio.get(i).setSelected(false);
            prezzo.add(new JLabel(String.valueOf(accessoriList.get(i).getPrezzo() + " €")));

            accessorio.get(i).setSize(200, 30);
            accessorio.get(i).setLocation(250, (200+(i*30)));
            prezzo.get(i).setLocation(450, (200+(i*30)));
            prezzo.get(i).setSize(200, 30);
            p.add(accessorio.get(i));
            p.add(prezzo.get(i));
        }



        localitaLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        localitaLbl.setSize(120, 20);
        localitaLbl.setLocation(50, 380);

        localitaBox = new JComboBox(localitaList.toArray());
        localitaBox.setFont(new Font("Arial", Font.PLAIN, 15));
        localitaBox.setSize(300, 20);
        localitaBox.setLocation(250, 380);

        p.add(localitaLbl);
        p.add(localitaBox);



        stazionePartenzaLbl.setFont(new Font("Arial", Font.PLAIN, 15));
        stazionePartenzaLbl.setSize(200, 20);
        stazionePartenzaLbl.setLocation(50, 430);

        stazionePartenzaBox = new JComboBox(stazioneList.toArray());
        stazionePartenzaBox.setFont(new Font("Arial", Font.PLAIN, 15));
        stazionePartenzaBox.setSize(300, 20);
        stazionePartenzaBox.setLocation(250, 430);

        p.add(stazionePartenzaLbl);
        p.add(stazionePartenzaBox);



        stazioneArrivoLbl.setFont(new Font("Arial", Font.PLAIN, 15));
        stazioneArrivoLbl.setSize(200, 20);
        stazioneArrivoLbl.setLocation(50, 480);

        stazioneArrivoBox = new JComboBox(stazioneList.toArray());
        stazioneArrivoBox.setFont(new Font("Arial", Font.PLAIN, 15));
        stazioneArrivoBox.setSize(300, 20);
        stazioneArrivoBox.setLocation(250, 480);

        p.add(stazioneArrivoLbl);
        p.add(stazioneArrivoBox);



        Dimension saveSize = saveButton.getPreferredSize();
        saveButton.setBounds(250, 550, saveSize.width, saveSize.height);
        Dimension annullaSize = annullaButton.getPreferredSize();
        annullaButton.setBounds(325, 550, annullaSize.width, annullaSize.height);

        p.add(annullaButton);
        p.add(saveButton);



        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String monthStartConvertedCB = null;
                String monthEndConvertedCB = null;

                if (e.getSource() == saveButton) {
                    String dateStart = dateStartBox.getSelectedItem().toString();
                    String monthStart = monthStartBox.getSelectedItem().toString();
                    switch(monthStart) {
                        case "Jan":
                            monthStartConvertedCB = "01";
                            break;

                        case "Feb":
                            monthStartConvertedCB = "02";
                            break;

                        case "Mar":
                            monthStartConvertedCB = "03";
                            break;

                        case "Apr":
                            monthStartConvertedCB = "04";
                            break;

                        case "May":
                            monthStartConvertedCB = "05";
                            break;

                        case "Jun":
                            monthStartConvertedCB = "06";
                            break;

                        case "July":
                            monthStartConvertedCB = "07";
                            break;

                        case "Aug":
                            monthStartConvertedCB = "08";
                            break;

                        case "Sep":
                            monthStartConvertedCB = "09";
                            break;

                        case "Oct":
                            monthStartConvertedCB = "10";
                            break;

                        case "Nov":
                            monthStartConvertedCB = "11";
                            break;

                        case "Dec":
                            monthStartConvertedCB = "12";
                            break;
                    }
                    String yearStart = yearStartBox.getSelectedItem().toString();
                    String dateStartTime = yearStart + "-" + monthStartConvertedCB + "-" + dateStart+ " 00:00:00";

                    String dateEnd = dateEndBox.getSelectedItem().toString();
                    String monthEnd = monthEndBox.getSelectedItem().toString();

                    switch(monthEnd) {
                        case "Jan":
                            monthEndConvertedCB = "01";
                            break;

                        case "Feb":
                            monthEndConvertedCB = "02";
                            break;

                        case "Mar":
                            monthEndConvertedCB = "03";
                            break;

                        case "Apr":
                            monthEndConvertedCB = "04";
                            break;

                        case "May":
                            monthEndConvertedCB = "05";
                            break;

                        case "Jun":
                            monthEndConvertedCB = "06";
                            break;

                        case "July":
                            monthEndConvertedCB = "07";
                            break;

                        case "Aug":
                            monthEndConvertedCB = "08";
                            break;

                        case "Sep":
                            monthEndConvertedCB = "09";
                            break;

                        case "Oct":
                            monthEndConvertedCB = "10";
                            break;

                        case "Nov":
                            monthEndConvertedCB = "11";
                            break;

                        case "Dec":
                            monthEndConvertedCB = "12";
                            break;
                    }

                    String yearEnd = yearEndBox.getSelectedItem().toString();
                    String dateEndTime = yearEnd + "-" + monthEndConvertedCB + "-" + dateEnd+ " 00:00:00";

                    SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date4= new Date(System.currentTimeMillis());

                    Utente loggedUser =  (Utente)Session.getInstance().get(Session.LOGGED_USER);

                    Mezzo mezzo = (Mezzo) mezzoBox.getSelectedItem();

                    Localita localita = (Localita) localitaBox.getSelectedItem();

                    Stazione stazionePartenza = (Stazione) stazionePartenzaBox.getSelectedItem();

                    Stazione stazioneArrivo = (Stazione) stazioneArrivoBox.getSelectedItem();

                    PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();

                    Prenotazione p = new Prenotazione();

                    int prezzoAccessori = 0;

                    ArrayList<Accessorio> accessoriToSave = new ArrayList<>();
                    for (int i = 0; i < accessoriList.size(); i++) {
                        if (accessorio.get(i).isSelected()) {
                            accessoriToSave.add(accessoriList.get(i));
                        }
                    }

                    for (int i = 0; i < accessoriToSave.size(); i++){
                        prezzoAccessori += accessoriToSave.get(i).getPrezzo();
                    }

                    int prezzoTotale = mezzo.getPrezzo() + prezzoAccessori;


                    p.setData_prenotazione(formatter.format(date4));
                    p.setData_inizio(dateStartTime);
                    p.setData_fine(dateEndTime);
                    p.setPrezzoFinale(prezzoTotale);
                    p.setCliente(loggedUser);
                    p.setMezzo(mezzo);
                    p.setLocalita(localita);
                    p.setStazione_partenza(stazionePartenza);
                    p.setStazione_arrivo(stazioneArrivo);
                    p.setAccessori(accessoriToSave);
                    p.setNum_posti_occupati(1);
                    p.setPagato(1);
                    p.setSharable(1);

                    int prenotazioneId = prenotazioneDAO.checkPrenotazione(p);

                    if ( prenotazioneId != 0 ) {

                        richiestaSharingFrame = new RichiestaSharingFrame(prenotazioneId, p);

                        instance.dispose();

                    } else {
                        int res = JOptionPane.showConfirmDialog(
                                null,
                                " Il prezzo totale è: " + prezzoTotale + ", salvare prenotazioe?");
                        if (res==JOptionPane.YES_OPTION){
                            prenotazioneDAO.save(p);
                            JOptionPane.showMessageDialog(null, "Prenotazione salvata!");

                            instance.dispose();

                        }
                    }
                }
            }
        });

        annullaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                instance.dispose();

            }
        });


        setBounds(300, 90, 700, 650);
        setResizable(false);
        setVisible(true);
        }

    }


/********************************************************************************************
 *
 * 1) prima di creare la prenotazione bisogna fare una query per vedere se esiste già un'altra prenotazione simile
 * 2) se esiste almeno una prenotazione identica dove in numero di posti disponibili non è ancora saturo  allora deve uscire un alert che indica all'
 *      utente se vuole prenotare in sharing mostrandogli chi sono gli altri clienti dello sharing
 * 3) se accetta la prenotazione in sharing
 ********************************************************************************************/
