package it.unisalento.pps1920.carsharing.view.messaggi;

import it.unisalento.pps1920.carsharing.dao.mysql.MessaggioDAO;
import it.unisalento.pps1920.carsharing.model.Messaggio;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Session;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WriteMessaggioFrame extends JFrame {

    public WriteMessaggioFrame() throws HeadlessException {

        super("Scrivi messaggio");

        JFrame f = new JFrame();
        JLabel testo = new JLabel("Invio messaggio:");
        testo.setBounds(140,10, 100,30);
        JTextField t1;
        t1 = new JTextField("Inserire destinatario qui");

        JButton invia= new JButton("Invia");
        JButton annulla= new JButton("Anulla");
        JTextArea area=new JTextArea("Inserisci il tuo messaggio");

        t1.setBounds(140,50, 200,30);

        area.setBounds(140,100, 200,200);
        invia.setSize(70, 40);
        invia.setLocation(140, 320);
        annulla.setSize(70, 40);
        annulla.setLocation(240, 320);

        f.setLayout(null);
        f.add(testo);
        f.add(t1);
        f.add(invia);
        f.add(annulla);

        invia.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (e.getSource() == invia) {

                    Utente loggedUser =  (Utente) Session.getInstance().get(Session.LOGGED_USER);

                    MessaggioDAO messaggioDAO = new MessaggioDAO();

                    Messaggio m = new Messaggio();

                    m.setMessaggio(area.getText());
                    m.setMittente(loggedUser.getEmail());
                    m.setDestinatario(t1.getText());

                    messaggioDAO.save(m);

                    JOptionPane.showMessageDialog(null," Messaggio inviato");

                    f.dispose();
                }
            }
        });

        f.add(area);
        f.setSize(500,500);
        f.setLayout(null);
        f.setVisible(true);
    }
}
