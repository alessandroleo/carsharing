package it.unisalento.pps1920.carsharing.view.messaggi;

import it.unisalento.pps1920.carsharing.dao.mysql.MessaggioDAO;
import it.unisalento.pps1920.carsharing.model.Messaggio;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Session;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ViewMessaggiFrame extends JFrame {

    JFrame instance;

    JLabel messaggioLbl;
    JLabel mittenteLbl;;
    JLabel nessunMessaggio = new JLabel("Nessun messaggio.");

    public MessaggioDAO messaggioDAO = new MessaggioDAO();
    public ArrayList<Messaggio> messaggioList;

    public ViewMessaggiFrame() throws HeadlessException {

        super("Lista messaggi");
        this.instance = this;
        this.setLayout(new FlowLayout());
        this.setSize(520, 550);
        JPanel container = new JPanel();

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setSize(500, 500);

        JScrollPane scroller = new JScrollPane(container);
        scroller.setPreferredSize(new Dimension(500, 500));

        this.setResizable(false);
        this.setVisible(true);
        this.add(scroller);
        scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        Utente loggedUser = (Utente) Session.getInstance().get(Session.LOGGED_USER);

        messaggioList = messaggioDAO.findAllByEmail(loggedUser.getEmail());

        if (messaggioList.size() != 0){

        for (int i = 0; i<messaggioList.size(); i++) {

            messaggioLbl = new JLabel("Messaggio: " + messaggioList.get(i).getMessaggio());
            messaggioLbl.setFont(new Font("Arial", Font.PLAIN, 20));

            mittenteLbl = new JLabel("Mittente: " + messaggioList.get(i).getMittente());
            mittenteLbl.setFont(new Font("Arial", Font.PLAIN, 25));

            container.add(mittenteLbl);
            container.add(messaggioLbl);
            container.add(Box.createRigidArea(new Dimension(0, 40)));



            }
        }

        else{
            nessunMessaggio.setFont(new Font("Arial", Font.PLAIN, 25));
            container.add(nessunMessaggio);
        }
    }
}
