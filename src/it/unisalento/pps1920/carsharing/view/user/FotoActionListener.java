package it.unisalento.pps1920.carsharing.view.user;

import it.unisalento.pps1920.carsharing.dao.mysql.UtenteDAO;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Constants;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class FotoActionListener implements ActionListener {


    private UserCreateFrame frame;

    //public byte[] image;
    public File selectedFile;

    public FotoActionListener(UserCreateFrame frame) {
        this.frame = frame;
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals("upload")) {

            JFileChooser file = new JFileChooser();
            file.setCurrentDirectory(new File(System.getProperty("user.home")));

            FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
            file.addChoosableFileFilter(filter);
            int result = file.showSaveDialog(null);

            if (result == JFileChooser.APPROVE_OPTION ) {
                 selectedFile = file.getSelectedFile();
            }
        } else if (e.getActionCommand().equals("save")) {

            UtenteDAO utenteDAO = new UtenteDAO();
            Utente u = new Utente();
            u.setNome(frame.nomeTf.getText());
            u.setCognome(frame.cognomeTf.getText());
            u.setEmail(frame.emailTf.getText());
            u.setPassword(frame.pswPf.getText());
            u.setTelefono(frame.telefonoTf.getText());
            u.setEta(Integer.parseInt(frame.etaTf.getText()));
            u.setResidenza(frame.residenzaTf.getText());
            u.setInput(selectedFile);
            u.setRuolo(Constants.ROLE_CLIENTE);

            utenteDAO.save(u);

            JOptionPane.showMessageDialog(null, " Utente salvato");

            frame.instance.dispose();

        }
    }
}
