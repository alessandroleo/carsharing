package it.unisalento.pps1920.carsharing.view.user;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.service.UserService;
import it.unisalento.pps1920.carsharing.util.Session;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class UserProfileFrame extends JFrame {

    JFrame instance;

    JLabel nomeLbl;
    JLabel cognomeLbl;
    JLabel etaLbl;
    JLabel emailLbl;
    JLabel telefonoLbl;
    JLabel residenzaLbl;

    public UserProfileFrame() throws HeadlessException, IOException {
        super("Profilo utente");
        this.instance = this;

        if (Session.getInstance().get(Session.LOGGED_USER) != null) {
            Utente loggedUser =  (Utente)Session.getInstance().get(Session.LOGGED_USER);

            nomeLbl = new JLabel("Nome: " + loggedUser.getNome());
            cognomeLbl = new JLabel("Cognome: " + loggedUser.getCognome());
            etaLbl = new JLabel("Eta: " + loggedUser.getEta());
            emailLbl = new JLabel("Email: " + loggedUser.getEmail());
            telefonoLbl = new JLabel("Telefono: " + loggedUser.getTelefono());
            residenzaLbl = new JLabel("Residenza: " + loggedUser.getResidenza());

            byte[] img = loggedUser.getFoto();
            JLabel picLabel = new JLabel(new ImageIcon(img));

        Container p = this.getContentPane();

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

            p.add(picLabel);

            p.add(Box.createRigidArea(new Dimension(0, 20)));

        nomeLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        p.add(nomeLbl);

            p.add(Box.createRigidArea(new Dimension(0, 30)));

        cognomeLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        p.add(cognomeLbl);

            p.add(Box.createRigidArea(new Dimension(0, 30)));

        etaLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        p.add(etaLbl);

            p.add(Box.createRigidArea(new Dimension(0, 30)));

        emailLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        p.add(emailLbl);

            p.add(Box.createRigidArea(new Dimension(0, 30)));

        telefonoLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        p.add(telefonoLbl);

            p.add(Box.createRigidArea(new Dimension(0, 30)));

        residenzaLbl.setFont(new Font("Arial", Font.PLAIN, 20));
        p.add(residenzaLbl);


            setBounds(300, 90, 400, 650);
            setResizable(false);
            setVisible(true);

        }
    }
}

