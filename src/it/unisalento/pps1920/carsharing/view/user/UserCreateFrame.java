package it.unisalento.pps1920.carsharing.view.user;

import it.unisalento.pps1920.carsharing.dao.mysql.UtenteDAO;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Constants;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class UserCreateFrame extends JFrame {

    JFrame instance;

    public String data1;
    public String data2;
    public String data3;
    public String data4;
    public String data5;
    public int data6;
    public String data7;
    public byte[] data8;

    JButton saveButton = new JButton("Salva");
    JButton annullaButton = new JButton("Annulla");

    JLabel nomeLab = new JLabel("Nome:");
    JTextField nomeTf = new JTextField(10);

    JLabel cognomeLab = new JLabel("Cognome:");
    JTextField cognomeTf = new JTextField(10);

    JLabel pswLab = new JLabel("Password:");
    JPasswordField pswPf = new JPasswordField(10);

    JLabel emailLab = new JLabel("Email:");
    JTextField emailTf = new JTextField(20);

    JLabel telefonoLab = new JLabel("Telefono:");
    JTextField telefonoTf = new JTextField(10);

    JLabel etaLab = new JLabel("Età:");
    JTextField etaTf = new JTextField(3);

    JLabel residenzaLab = new JLabel("Indirizzo:");
    JTextField residenzaTf = new JTextField(50);

    JButton caricaFotoBtn = new JButton("Carica Foto");



    public UserCreateFrame() throws HeadlessException {
        super("Registrazione");
        this.instance = this;

        Container p = this.getContentPane();

        p.setLayout(null);


        caricaFotoBtn.setBounds(125, 400, 150, 40);
  ;
        p.add(caricaFotoBtn);

        nomeLab.setFont(new Font("Arial", Font.PLAIN, 20));
        nomeLab.setSize(100, 20);
        nomeLab.setLocation(50, 50);
        nomeTf.setFont(new Font("Arial", Font.PLAIN, 15));
        nomeTf.setSize(190, 20);
        nomeTf.setLocation(150, 50);

        p.add(nomeLab);
        p.add(nomeTf);



        cognomeLab.setFont(new Font("Arial", Font.PLAIN, 20));
        cognomeLab.setSize(100, 20);
        cognomeLab.setLocation(50, 100);
        cognomeTf.setFont(new Font("Arial", Font.PLAIN, 15));
        cognomeTf.setSize(190, 20);
        cognomeTf.setLocation(150, 100);

        p.add(cognomeLab);
        p.add(cognomeTf);



        pswLab.setFont(new Font("Arial", Font.PLAIN, 20));
        pswLab.setSize(100, 20);
        pswLab.setLocation(50, 150);
        pswPf.setFont(new Font("Arial", Font.PLAIN, 15));
        pswPf.setSize(190, 20);
        pswPf.setLocation(150, 150);

        p.add(pswLab);
        p.add(pswPf);



        emailLab.setFont(new Font("Arial", Font.PLAIN, 20));
        emailLab.setSize(100, 20);
        emailLab.setLocation(50, 200);
        emailTf.setFont(new Font("Arial", Font.PLAIN, 15));
        emailTf.setSize(190, 20);
        emailTf.setLocation(150, 200);

        p.add(emailLab);
        p.add(emailTf);



        telefonoLab.setFont(new Font("Arial", Font.PLAIN, 20));
        telefonoLab.setSize(100, 20);
        telefonoLab.setLocation(50, 250);
        telefonoTf.setFont(new Font("Arial", Font.PLAIN, 15));
        telefonoTf.setSize(190, 20);
        telefonoTf.setLocation(150, 250);


        p.add(telefonoLab);
        p.add(telefonoTf);



        etaLab.setFont(new Font("Arial", Font.PLAIN, 20));
        etaLab.setSize(100, 20);
        etaLab.setLocation(50, 300);
        etaTf.setFont(new Font("Arial", Font.PLAIN, 15));
        etaTf.setSize(190, 20);
        etaTf.setLocation(150, 300);


        p.add(etaLab);
        p.add(etaTf);



        residenzaLab.setFont(new Font("Arial", Font.PLAIN, 20));
        residenzaLab.setSize(100, 20);
        residenzaLab.setLocation(50, 350);
        residenzaTf.setFont(new Font("Arial", Font.PLAIN, 15));
        residenzaTf.setSize(190, 20);
        residenzaTf.setLocation(150, 350);


        p.add(residenzaLab);
        p.add(residenzaTf);



        Dimension saveSize = saveButton.getPreferredSize();
        saveButton.setBounds(125, 450, saveSize.width, saveSize.height);

        Dimension annullaSize = annullaButton.getPreferredSize();
        annullaButton.setBounds(200, 450, annullaSize.width, annullaSize.height);

        p.add(annullaButton);
        p.add(saveButton);

        FotoActionListener listener = new FotoActionListener(this);

        saveButton.addActionListener(listener);
        caricaFotoBtn.addActionListener(listener);

        saveButton.setActionCommand("save");
        caricaFotoBtn.setActionCommand("upload");

        annullaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                instance.dispose();

            }
        });

        setBounds(300, 90, 400, 550);
        setResizable(false);
        setVisible(true);
    }
}
