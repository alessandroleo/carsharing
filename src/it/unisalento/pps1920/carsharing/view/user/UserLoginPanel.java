package it.unisalento.pps1920.carsharing.view.user;

import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.service.UserService;
import it.unisalento.pps1920.carsharing.util.Session;
import it.unisalento.pps1920.carsharing.view.MainMenuWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class UserLoginPanel extends JPanel {

    private UserService userService = new UserService();

    Utente loggedUser = (Utente) Session.getInstance().get(Session.LOGGED_USER);

    public JButton signinButton = new JButton("Registrati");
    public JButton signupButton = new JButton("Accedi");
    public JButton signoutButton = new JButton("Logout");
    public JButton viewProfileBtn;
    public JFrame userForm;
    public JFrame loginForm;
    public JFrame profileForm;

    public UserLoginPanel() {
        super();

        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.black));

        JPanel mainCenterPanel = new JPanel();
        mainCenterPanel.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
        mainCenterPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        JPanel mainRightPanel = new JPanel();
        mainRightPanel.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
        mainRightPanel.setLayout(new FlowLayout());

        JLabel appNameLab = new JLabel("CAR SHARING BY LEO & COLELLA");
        appNameLab.setFont(new Font("Arial", Font.PLAIN, 20));
        mainCenterPanel.add(appNameLab);
        this.add(mainCenterPanel, BorderLayout.CENTER);

        String role;

        if (Session.getInstance().get(Session.LOGGED_USER) != null) {

            switch (loggedUser.getRuolo()) {
                case "ROLE_ADMIN":
                    role = "Admin";
                    break;

                case "ROLE_CLIENTE":
                    role = "Cliente";
                    break;

                case "ROLE_OPERATORE_SPORTELLO":
                    role = "Operatore di sportello";
                    break;

                case "ROLE_ADDETTO":
                    role = "Addetto";
                    break;

                default:
                    throw new IllegalStateException("Unexpected value: " + loggedUser.getRuolo());
            }


            viewProfileBtn = new JButton("Visualizza profilo " + "(" + role + ")");

            viewProfileBtn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        profileForm = new UserProfileFrame();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });

            signoutButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    userService.logout();
                    ((MainMenuWindow)MainMenuWindow.instance).drawMain();
                }
            });

            //mainRightPanel.add(userLabel);
            mainRightPanel.add(viewProfileBtn);
            mainRightPanel.add(signoutButton);

            this.add(mainRightPanel, BorderLayout.EAST);
        } else {
            signinButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    loginForm = new UserCreateFrame();
                }
            });

            signupButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    userForm = new UserLoginFrame();
                }
            });

            mainRightPanel.add(signinButton);
            mainRightPanel.add(signupButton);
            this.add(mainRightPanel, BorderLayout.EAST);
        }

        setVisible(true);
    }

}
