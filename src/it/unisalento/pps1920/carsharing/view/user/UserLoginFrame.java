package it.unisalento.pps1920.carsharing.view.user;

import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.service.UserService;
import it.unisalento.pps1920.carsharing.view.MainMenuWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserLoginFrame extends JFrame {

    private UserService userService = new UserService();

    JFrame instance;
    JButton loginButton = new JButton("Accedi");
    JButton annullaButton = new JButton("Annulla");

    JLabel emailLab = new JLabel("Email:");
    JTextField emailTf = new JTextField(20);

    JLabel passwordLab = new JLabel("Password:");
    JPasswordField passwordPf = new JPasswordField(10);

    public UserLoginFrame() throws HeadlessException {
        super("Accedi");
        this.instance = this;

        Container p = this.getContentPane();

        p.setLayout(null);

        p.add(emailLab);
        p.add(emailTf);
        emailLab.setFont(new Font("Arial", Font.PLAIN, 20));
        emailLab.setSize(100, 20);
        emailLab.setLocation(50, 50);
        emailTf.setFont(new Font("Arial", Font.PLAIN, 15));
        emailTf.setSize(190, 20);
        emailTf.setLocation(150, 50);

        p.add(passwordLab);
        p.add(passwordPf);
        passwordLab.setFont(new Font("Arial", Font.PLAIN, 20));
        passwordLab.setSize(100, 20);
        passwordLab.setLocation(50, 100);
        passwordPf.setFont(new Font("Arial", Font.PLAIN, 15));
        passwordPf.setSize(190, 20);
        passwordPf.setLocation(150, 100);

        Dimension saveSize = loginButton.getPreferredSize();
        loginButton.setBounds(125, 150, saveSize.width, saveSize.height);
        Dimension annullaSize = annullaButton.getPreferredSize();
        annullaButton.setBounds(200, 150, annullaSize.width, annullaSize.height);
        p.add(annullaButton);
        p.add(loginButton);

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Utente u = userService.login(emailTf.getText(), passwordPf.getText());

                if (u != null) {
                    ((MainMenuWindow)MainMenuWindow.instance).drawMain();
                    instance.dispose();
                } else {
                    JOptionPane.showMessageDialog(null,"Email o password errate!");
                }
            }
        });

        annullaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                instance.dispose();
            }
        });

        setBounds(300, 90, 400, 250);
        setResizable(false);
        setVisible(true);
    }


}
