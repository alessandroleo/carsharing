package it.unisalento.pps1920.carsharing.view.addetto;

import it.unisalento.pps1920.carsharing.dao.mysql.AccessorioDAO;
import it.unisalento.pps1920.carsharing.dao.mysql.PrenotazioneDAO;
import it.unisalento.pps1920.carsharing.model.Accessorio;
import it.unisalento.pps1920.carsharing.model.Prenotazione;
import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Session;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ViewPrenotazioniAddetto extends JFrame {

    JFrame instance;

    JLabel dataPrenotazioneLbl;
    JLabel dataInizioLbl;
    JLabel dataFineLbl;
    JLabel clienteLbl;
    JLabel mezzoLbl;
    JLabel localitaLbl;
    JLabel stazionePartenzaLbl;
    JLabel stazioneArrivoLbl;
    JLabel toChargeLbl;
    JLabel accessoriLbl;
    JLabel nessunAccessorio;

    JButton accessoriBtn;

    JCheckBox accessoriatoCb;

    Utente loggedUser =  (Utente) Session.getInstance().get(Session.LOGGED_USER);

    public PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();
    public ArrayList<Prenotazione> prenotazioneList;
    public ArrayList<Utente> clientiList;

    public AccessorioDAO accessorioDAO = new AccessorioDAO();
    public ArrayList<Accessorio> accessorioList;

    public ViewPrenotazioniAddetto() throws HeadlessException {

        super("Lista prenotazioni");

        this.instance = this;

        this.setSize(620, 550);

        JPanel container = new JPanel();

        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setSize(600, 500);

        JScrollPane scroller = new JScrollPane(container);
        scroller.setPreferredSize(new Dimension(600, 500));

        this.setResizable(false);
        this.setVisible(true);
        this.add(scroller);
        scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        prenotazioneList = prenotazioneDAO.findAllByStazioneId(loggedUser.getStazione().getId());


        for (int i = 0; i < prenotazioneList.size(); i++) {

            dataPrenotazioneLbl = new JLabel("Data di avvenuta prenotazione: " + prenotazioneList.get(i).getData_prenotazione());
            dataPrenotazioneLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            dataInizioLbl = new JLabel("Data di inizio: " + prenotazioneList.get(i).getData_inizio());
            dataInizioLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            dataFineLbl = new JLabel("Data di fine: " + prenotazioneList.get(i).getData_fine());
            dataFineLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            mezzoLbl = new JLabel("Mezzo: " + prenotazioneList.get(i).getMezzo().getModello().getNome() + " " +
                    prenotazioneList.get(i).getMezzo().getMarca().getNome() + " " +
                    prenotazioneList.get(i).getMezzo().getMotorizzazione().getTipo());
            mezzoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            localitaLbl = new JLabel("Località scelta: " + prenotazioneList.get(i).getLocalita().getCitta());
            localitaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            stazionePartenzaLbl = new JLabel("Stazione di partenza: "+ prenotazioneList.get(i).getStazione_partenza().getNome());
            stazionePartenzaLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            stazioneArrivoLbl = new JLabel("Stazione di arrivo: "+ prenotazioneList.get(i).getStazione_arrivo().getNome());
            stazioneArrivoLbl.setFont(new Font("Arial", Font.PLAIN, 15));

            accessoriatoCb = new JCheckBox("Mezzo accessoriato");
            accessoriatoCb.setEnabled(false);

            if (prenotazioneList.get(i).getAccessoriato() == 1) {
                accessoriatoCb.setSelected(true);
            }

            container.add(dataPrenotazioneLbl);
            container.add(dataInizioLbl);
            container.add(dataFineLbl);

            clientiList = prenotazioneDAO.findAllClientiByPrenotazioneId(prenotazioneList.get(i).getId());
            ArrayList<JLabel> clientiLbl = new ArrayList<JLabel>();

            for (int j = 0; j < clientiList.size(); j++){

                clientiLbl.add(new JLabel("Clienti: " + clientiList.get(j).getNome()));
                clientiLbl.get(j).setFont(new Font("Arial", Font.PLAIN, 15));
                container.add(clientiLbl.get(j));

            }

            container.add(mezzoLbl);
            container.add(localitaLbl);
            container.add(stazionePartenzaLbl);
            container.add(stazioneArrivoLbl);

            container.add(Box.createRigidArea(new Dimension(0, 10)));

            toChargeLbl = new JLabel("Accessori da caricare: ");
            toChargeLbl.setFont(new Font("Arial", Font.PLAIN, 15));
            container.add(toChargeLbl);

            accessorioList = accessorioDAO.findAllByPrenotazioneId(prenotazioneList.get(i).getId());

            if (accessorioList.size() != 0) {

                for (int j = 0; j < accessorioList.size(); j++) {

                    accessoriLbl = new JLabel(" - " + accessorioList.get(j).getNome());
                    accessoriLbl.setFont(new Font("Arial", Font.PLAIN, 15));

                    container.add(accessoriLbl);

                }

                container.add(Box.createRigidArea(new Dimension(0, 10)));

                accessoriBtn = new JButton("Carica accessori");
                container.add(accessoriBtn);
                container.add(accessoriatoCb);

                accessoriBtn.addActionListener(new SetAccessoriActionListener(i));
            }

            else {

                nessunAccessorio = new JLabel("Nessuno");
                nessunAccessorio.setFont(new Font("Arial", Font.PLAIN, 15));

                container.add(nessunAccessorio);
            }

            container.add(Box.createRigidArea(new Dimension(0, 30)));

        }
    }

    private class SetAccessoriActionListener implements ActionListener {
        private int index;

        public SetAccessoriActionListener(int index) {
            this.index = index;
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            Prenotazione p = prenotazioneList.get(index);
            p.setAccessoriato(1);
            prenotazioneDAO.save(p);

            JOptionPane.showMessageDialog(null, "Oggetti caricati sul mezzo");

        }
    }
}
