package it.unisalento.pps1920.carsharing.view;

import it.unisalento.pps1920.carsharing.model.Utente;
import it.unisalento.pps1920.carsharing.util.Constants;
import it.unisalento.pps1920.carsharing.util.Session;
import it.unisalento.pps1920.carsharing.view.addetto.AddettoUserHome;
import it.unisalento.pps1920.carsharing.view.operatore.OperatoreUserHome;
import it.unisalento.pps1920.carsharing.view.admin.AdminFrame;
import it.unisalento.pps1920.carsharing.view.mezzo.MezziHome;
import it.unisalento.pps1920.carsharing.view.prenotazione.MenuPrenotazioniPanel;
import it.unisalento.pps1920.carsharing.view.user.UserLoginPanel;

import javax.swing.*;
import java.awt.*;

public class MainMenuWindow extends JFrame {

    public static JFrame instance;
    private Container container;

    public MainMenuWindow() {
        super("Car Sharing App");
        instance = this;

        container = this.getContentPane();
        container.setLayout(new BorderLayout());
        drawMain();

        JOptionPane.showMessageDialog(null,"Benvenuti in car sharing by Leo & Colella! " +  "\r\n" +
                "Qui potrai visualizzare il nostro catalogo mezzi e le offerte disponibili, " +
                "per poter effettuare una prenotazione, " +
                "ti invitiamo ad iscriverti e a diventare un cliente. " +  "\r\n" +
                "Grazie per averci scelto!");

    }

    public void drawMain() {
        container.removeAll();

        JPanel userPanel = new UserLoginPanel();
        JPanel mainPanel;
        container.add(userPanel, BorderLayout.NORTH);

        Utente loggedUser = (Utente) Session.getInstance().get(Session.LOGGED_USER);
        String ruolo;
        if (loggedUser != null && loggedUser.getRuolo() != null) {
            ruolo = loggedUser.getRuolo();
        } else {
            ruolo = Constants.ROLE_ANONYMOUS;
        }

        switch (ruolo) {
            case Constants.ROLE_ANONYMOUS:
                mainPanel = drawAnonymousUserHome();
                break;

            case Constants.ROLE_CLIENTE:
                mainPanel = drawClienteUserHome();
                break;

            case Constants.ROLE_ADMIN:
                mainPanel = drawAdminUserHome();
                break;

            case Constants.ROLE_ADDETTO:
                mainPanel = drawAddettoUserHome();
                break;

            case Constants.ROLE_OPERATORE_SPORTELLO:
                mainPanel = drawOperatoreSportelloUserHome();
                break;

            default:
                mainPanel = drawAnonymousUserHome();
        }

        container.add(mainPanel, BorderLayout.CENTER);

        this.setSize(800,800);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((int)((dim.getWidth()-this.getWidth())/2),(int)((dim.getHeight()-this.getHeight())/2));

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setVisible(true);
    }

    private JPanel drawAnonymousUserHome() {
        JPanel mezzoPanel = new MezziHome();
        return mezzoPanel;
    }

    private JPanel drawAdminUserHome() {
        JPanel adminFrame = new AdminFrame();
        return adminFrame;
    }

    private JPanel drawClienteUserHome() {
        JPanel ClientePanel = new MenuPrenotazioniPanel();
        return ClientePanel;
    }

    private JPanel drawOperatoreSportelloUserHome() {
        JPanel OperatorePanel = new OperatoreUserHome();
        return OperatorePanel;
    }

    private JPanel drawAddettoUserHome() {
        JPanel AddettoPanel = new AddettoUserHome();
        return AddettoPanel;
    }
}
