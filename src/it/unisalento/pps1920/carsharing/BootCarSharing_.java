package it.unisalento.pps1920.carsharing;

import it.unisalento.pps1920.carsharing.business.PrenotazioneBusiness;
import it.unisalento.pps1920.carsharing.dao.interfaces.*;
import it.unisalento.pps1920.carsharing.dao.mysql.*;
import it.unisalento.pps1920.carsharing.model.Localita;
import it.unisalento.pps1920.carsharing.model.Mezzo;
import it.unisalento.pps1920.carsharing.model.Prenotazione;
import it.unisalento.pps1920.carsharing.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class BootCarSharing_ {

    /*public static void main(String args[]) {

        ILocalitaDAO lDao = new LocalitaDAO();

        System.out.println("CHIAMO METODO findAll");
        for(Localita loc : lDao.findAll()) {
            System.out.println("ID: "+loc.getId());
            System.out.println("Nome: "+loc.getCitta());
            System.out.println("Lat: "+loc.getLatitudine());
            System.out.println("Lon: "+loc.getLongitudine());
            System.out.println("--------------");
        }

        System.out.println("CHIAMO METODO findById");
        Localita loc = lDao.findById(1);
        if(loc!=null) {
            System.out.println("ID: " + loc.getId());
            System.out.println("Nome: " + loc.getCitta());
            System.out.println("Lat: " + loc.getLatitudine());
            System.out.println("Lon: " + loc.getLongitudine());
            System.out.println("--------------");
        }

        IPrenotazioneDAO pDao = new PrenotazioneDAO();
        ArrayList<Prenotazione> lista = pDao.findAll();

        for(Prenotazione p : lista) {
            System.out.println("ID CLIENTE: "+p.getCliente().getId());
            System.out.println("AUTO "+p.getMezzo().getModello().getNome()+" TARGATA "+p.getMezzo().getTarga());
            System.out.println("STAZIONE DI PARTENZA: "+p.getPartenza().getNome());
            System.out.println("STAZIONE DI ARRIVO: "+p.getArrivo().getNome());
            System.out.println("POSTI OCCUPATI: "+p.getNumPostiOccupati());
        }


        Prenotazione p = new Prenotazione();
        p.setDataInizio(DateUtil.dateTimeFromString("2019-12-03 09:00:00"));
        p.setDataFine(DateUtil.dateTimeFromString("2019-12-06 19:00:00"));
        p.setData(new Date());
        IClienteDAO cDao = new ClienteDAO();
        p.setCliente(cDao.findById(3));
        p.setNumPostiOccupati(1);
        p.setLocalita(lDao.findById(1));
        IStazioneDAO sDao = new StazioneDAO();
        p.setPartenza(sDao.findById(1));
        p.setArrivo(sDao.findById(1));
        IMezzoDAO mDao = new MezzoDAO();
        p.setMezzo(mDao.findById(2));
        PrenotazioneBusiness.getInstance().inviaPrenotazione(p);





    }*/

    public static void main(String args[]) {


        /*MezzoDAO mezzoDAO = new MezzoDAO();

        Mezzo mezzo1 = mezzoDAO.findById(1);
        System.out.println("Mezzo (id: " + mezzo1.getId() + " - targa: " + mezzo1.getTarga() + ")");

        Mezzo mezzo2 = mezzoDAO.findById(2);
        System.out.println("Mezzo (id: " + mezzo2.getId() + " - targa: " + mezzo2.getTarga() + ")");

        ArrayList<Mezzo> mezzi = mezzoDAO.findAll();*/

        /*LocalitaDAO localitaDAO = new LocalitaDAO();

        Localita localita1 = new Localita();
        localita1.setCitta("Milano");
        localita1.setLatitudine(0.0);
        localita1.setLongitudine(0.0);

        System.out.println("Saving...");
        localitaDAO.save(localita1);
        System.out.println("Saved");

        ArrayList<Localita> localitaList = localitaDAO.findAll();
        for (int i=0; i<localitaList.size(); i++) {
            Localita loc = localitaList.get(i);
            System.out.println(i+") citta: "+ loc.getCitta() + " lat: " + loc.getLatitudine() + " lng: " + loc.getLongitudine() );
        }

        Localita milanoLoc = localitaDAO.findById(3);
        milanoLoc.setCitta("Milano - updated");
        localitaDAO.save(milanoLoc);

        localitaList = localitaDAO.findAll();
        for (int i=0; i<localitaList.size(); i++) {
            Localita loc = localitaList.get(i);
            System.out.println(i+") citta: "+ loc.getCitta() + " lat: " + loc.getLatitudine() + " lng: " + loc.getLongitudine() );
        }

        for (int i=0; i<localitaList.size(); i++) {
            Localita loc = localitaList.get(i);
            localitaDAO.delete(loc);
            System.out.println("Delete localita ID: " + loc.getId());
        }
*/
        Scanner scanner = new Scanner(System.in);
        int risp;
        do {
            System.out.println("*******************************************************");
            System.out.println("*                 LEO CAR SHARING                     *");
            System.out.println("*******************************************************");
            System.out.println("");
            System.out.println("Menu");
            System.out.println("1) Visualizza tutte localita");
            System.out.println("2) Visualizza localita");
            System.out.println("3) Aggiungi localita");
            System.out.println("4) Modifica localita");
            System.out.println("5) Elimina localita");
            System.out.println("0) ESCI");
            System.out.println("");
            System.out.println("Seleziona una voce:");
            risp = scanner.nextInt();

            if (risp == 1) {
                viewAllLocalita();
            } else if (risp == 2) {
                viewAllLocalita();
                System.out.println("Quale localita vuoi visualizzare (seleziona l'ID dalla lista):");
                int index = scanner.nextInt();
                viewLocalita(index);
            } else if (risp == 3) {
                addLocalita();
            }
        } while (risp != 0);
        System.out.println("CIAO ;)");
    }

    public static void viewAllLocalita() {
        LocalitaDAO localitaDAO = new LocalitaDAO();

        ArrayList<Localita> localitaList = localitaDAO.findAll();
        System.out.println("Lista localita:");
        for (int i=0; i<localitaList.size(); i++) {
            Localita loc = localitaList.get(i);
            System.out.println(i+1+") ID: "+ loc.getId()+ " citta: "+ loc.getCitta() + " lat: " + loc.getLatitudine() + " lng: " + loc.getLongitudine() );
        }
    }

    public static void viewLocalita(int index) {
        LocalitaDAO localitaDAO = new LocalitaDAO();

        Localita localita = localitaDAO.findById(index);
        System.out.println("Visualizza localita:");

        System.out.print("Citta: ");
        System.out.println(localita.getCitta());
    }

    public static void addLocalita() {
        Scanner scanner = new Scanner(System.in);
        LocalitaDAO localitaDAO = new LocalitaDAO();

        System.out.println("Inserisci una nuova localita:");
        System.out.println("Inserisci la citta:");
        String citta = scanner.next();

        Localita localita = new Localita();
        localita.setCitta(citta);
        localita.setLatitudine(0.0);
        localita.setLongitudine(0.0);

        localitaDAO.save(localita);
    }

    /*public static void editPerson(int index) {
        Scanner scanner = new Scanner(System.in);
        Person person = Person.getPerson(index-1);

        System.out.println("Modifica persona (nome: "+person.getFirstName()+" cognome: "+person.getLastName()+"):");
        System.out.println("Modifica nome ("+person.getFirstName()+"):");
        String firstName = scanner.next();
        System.out.println("Modifica cognome ("+person.getLastName()+"):");
        String lastName = scanner.next();
        System.out.println("Modifica emai ("+person.getEmail()+")l");
        String email = scanner.next();
        System.out.println("Modifica codice fiscal ("+person.getFiscalCode()+")e");
        String fiscalCode = scanner.next();
        System.out.println("Modifica indirizzo ("+person.getAddress()+"):");
        String address = scanner.next();
        System.out.println("Modifica città ("+person.getCity()+"):");
        String city = scanner.next();
        System.out.println("Modifica nazione ("+person.getCountry()+"):");
        String country = scanner.next();
        System.out.println("Modifica genere (M/F) ("+person.getGenre()+"):");
        String genre = scanner.next();
        System.out.println("Modifica età ("+person.getAge()+"):");
        int age = scanner.nextInt();
        System.out.println("Modifica numero di cellulare ("+person.getPhone()+"):");
        String phone = scanner.next();
        System.out.println("Modifica numero della patente ("+person.getPatentNuber()+"):");
        String drivingLicenseNumber = scanner.next();

        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setEmail(email);
        person.setFiscalCode(fiscalCode);
        person.setAddress(address);
        person.setCity(city);
        person.setCountry(country);
        person.setGenre(genre);
        person.setAge(age);
        person.setPhone(phone);
        person.setPatentNuber(drivingLicenseNumber);

        Person.editPerson( (index -1), person);
    }*/
}
