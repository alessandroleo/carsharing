package it.unisalento.pps1920.carsharing.util;

import java.util.HashMap;

public class Session {

    private static Session instance;

    private HashMap<String, Object> map = new HashMap<String, Object>();

    //definisco le chiavi
    public static final String LOGGED_USER = "LOGGED_USER";

    public static synchronized Session getInstance() {
        if(instance == null)
            instance = new Session();
        return instance;
    }

    private Session() {

    }

    public void put(String key, Object value) {
        map.put(key, value);
    }

    public Object get(String key) {
        return map.get(key);
    }

    public void remove(String key) {
        map.remove(key);
    }
}
