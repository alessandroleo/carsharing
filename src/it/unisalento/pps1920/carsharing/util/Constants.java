package it.unisalento.pps1920.carsharing.util;

public class Constants {
    public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_CLIENTE = "ROLE_CLIENTE";
    public static final String ROLE_OPERATORE_SPORTELLO = "ROLE_OPERATORE_SPORTELLO";
    public static final String ROLE_ADDETTO = "ROLE_ADDETTO";

    public static final String DB_USER = "root";
    public static final String PASSWORD = "0833509162a";
    public static final String DB_NAME = "mydb";
}
